/*
 * signal.cc
 *
 *  Created on: Nov 5, 2020
 *      Author: kolos
 */

#include <unistd.h>

#include <map>

#include <ipc/signal.h>

using namespace daq::ipc;

typedef void (*SignalHandler)(int);

volatile std::sig_atomic_t signal::m_signal;

int signal::wait_for(std::vector<int> const& signums,
        uint32_t precision_ms) noexcept
{
    std::map<int, SignalHandler> old_handlers;
    for (auto it = signums.begin(); it != signums.end(); ++it) {
        SignalHandler sh = std::signal(*it, signalHandler);
        if (sh != SIG_ERR) {
            old_handlers[*it] = sh;
        }
    }

    m_signal = 0;

    while (!m_signal) {
        ::usleep(precision_ms);
    }

    for (auto it = old_handlers.begin(); it != old_handlers.end(); ++it) {
        std::signal(it->first, it->second);
    }

    return m_signal;
}

void signal::signalHandler(int signum)
{
    m_signal = signum;
}
