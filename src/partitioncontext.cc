#include <ipc/partitioncontext.h>
#include <ipc/exceptions.h>

namespace
{
    ipc::servant::PartitionContext * get_info( ipc::servant_ptr servant )
    {
        if ( CORBA::is_nil( servant ) ) {
            return 0;
        }
        
        try {
            return servant -> partition_context();
        }
        catch ( CORBA::SystemException & ex ) {
            return 0;
        }
    }
}

IPCPartitionContext::IPCPartitionContext(ipc::servant::PartitionContext * p)
: ipc::servant::PartitionContext_var( p ),
  m_valid( operator->() ),
  m_partition( m_valid ? (const char*)in().partition : "" ),
  m_name( m_valid ? (const char*)in().name : "" )
{
  out();
}

IPCPartitionContext::IPCPartitionContext( ipc::servant_ptr servant )
  : IPCPartitionContext( get_info( servant ) )
{ }

IPCPartitionContext::IPCPartitionContext( POA_ipc::servant & servant )
  : IPCPartitionContext( servant.partition_context() )
{ }
