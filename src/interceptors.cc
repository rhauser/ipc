/**
*	interceptors.cc
*	Base class for loading interceptors as plugins.
*	see pluginfactory.h
*
*	Author:  Serguei Kolos
*	Created: 21 August 2006
*/

#include <vector>
#include <omniORB4/CORBA.h>
#include <omniORB4/omniInterceptors.h>
#include <omniORB4/callDescriptor.h>
#include <omniORB4/internal/giopStrand.h>
#include <omniORB4/internal/giopStream.h>
#include <omniORB4/internal/giopRope.h>
#include <omniORB4/internal/GIOP_S.h>
#include <omniORB4/internal/GIOP_C.h>

#include <ers/ers.h>

#include <ipc/exceptions.h>
#include <ipc/interceptors.h>
#include <ipc/pluginfactory.h>
#include <ipc/singleton.h>

namespace ipc
{    
    template <class T>
    struct Parameters;
    
    template <>
    struct Parameters<IPCClientInterceptor>
    {
    	static const char * environment_name() { return "TDAQ_IPC_CLIENT_INTERCEPTORS"; }
    	static const char * default_value() { return "ipcproxyicp:ipcclicp"; }
    };
    
    template <>
    struct Parameters<IPCServerInterceptor>
    {
    	static const char * environment_name() { return "TDAQ_IPC_SERVER_INTERCEPTORS"; }
    	static const char * default_value() { return "ipcsrvicp"; }
    };
    
    class Interceptors
    {	
      template <class> friend class ::IPCSingleton;
      
      public:
        void initialize();
        void shutdown();
        
      private:
	Interceptors();
	~Interceptors();
        
        static bool client_interceptor( omni::omniInterceptors::clientSendRequest_T::info_T & info )
	{
	    IPCSingleton<Interceptors>::instance().execute_client_interceptors( info );
	    return true;
	}

	static bool server_interceptor( omni::omniInterceptors::serverReceiveRequest_T::info_T & info )
	{
	    IPCSingleton<Interceptors>::instance().execute_server_interceptors( info );
	    return true;
	}
        
        void execute_client_interceptors( omni::omniInterceptors::clientSendRequest_T::info_T & info )
        {
	    for ( size_t i = 0; i < client_interceptors_.size(); i++ ) {
		client_interceptors_[i]->sendRequest(  *info.giop_c.ior(),
                					info.giop_c.ior()->repositoryID(),
                                                        info.giop_c.operation(),
                                                        info.service_contexts );
	    }
	}

	void execute_server_interceptors( omni::omniInterceptors::serverReceiveRequest_T::info_T & info )
        {
	    for ( size_t i = 0; i < server_interceptors_.size(); i++ ) {
		server_interceptors_[i]->receiveRequest( info.giop_s.targetAddress().ior.type_id,
                					 info.giop_s.operation_name(),
                                                         info.giop_s.service_contexts( ) );
	    }
	}
	
        void destroy();
	        
        template <class T>
        void load( std::vector<T*> & interceptors_ );
        
        std::vector<IPCClientInterceptor*> 	client_interceptors_;
	std::vector<IPCServerInterceptor*> 	server_interceptors_;
    };

    Interceptors::Interceptors()
    {
    }
    
    Interceptors::~Interceptors()
    {
    	shutdown();
    }
    
    void Interceptors::initialize()
    {
	load( client_interceptors_ );
	load( server_interceptors_ );
        
        omni::omniInterceptors* interceptors = omniORB::getInterceptors();
	interceptors->serverReceiveRequest.add(server_interceptor);
	interceptors->clientSendRequest.add(client_interceptor);
    }

    void Interceptors::shutdown()
    {
        for ( size_t i = 0; i < client_interceptors_.size(); i++ ) {
	    delete client_interceptors_[i];
	}
        client_interceptors_.clear();
        
	for ( size_t i = 0; i < server_interceptors_.size(); i++ ) {
	    delete server_interceptors_[i];
	}
        server_interceptors_.clear();
    }
    
    void Interceptors::destroy()
    {
        delete this;
    }
    
    template <class T>
    void Interceptors::load( std::vector<T*> & interceptors_ )
    {
	const char * var = getenv( Parameters<T>::environment_name() );
	if ( !var )
	{
            var = Parameters<T>::default_value();
        }
        
	std::string plugins(var);
	plugins += ':';
	std::string::size_type start = 0, end;
	while( ( end = plugins.find( ':', start ) ) != std::string::npos )
	{
	    std::string plugin = plugins.substr( start, end - start );
	    start = end + 1;

	    if ( plugin.empty() )
		continue;
	    try {
		interceptors_.push_back(
			IPCSingleton< IPCPluginFactory<T> >::instance().create( plugin ) );
	    }
	    catch( daq::ipc::PluginException & ex ) {
            	ers::error( ex );
	    }
	    catch( daq::ipc::IgnorePluginException & ex ) {
            	ers::debug( ex, 1 );
	    }
            catch (...) {
            	ers::error( daq::ipc::PluginException( ERS_HERE, plugin,
            		"unknown exception in the plugin constructor" ) );
            }
	}
    }
    
    void initialize_interceptors()
    {
	IPCSingleton<ipc::Interceptors>::instance().initialize();
    }
    
    void destroy_interceptors()
    {
	IPCSingleton<ipc::Interceptors>::instance().shutdown();
    }
}
