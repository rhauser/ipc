#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include <ers/ers.h>

#include <ipc/interceptors.h>
#include <ipc/exceptions.h>

#define IPC_SECURE_SHUTDOWN 0x1001

class IPCClientDefaultInterceptor : public IPCClientInterceptor
{
    void sendRequest(	const omniIOR & ,
    			const char * interface_id,
    			const char * operation_id,
			IOP::ServiceContextList & );
};

void
IPCClientDefaultInterceptor::sendRequest( const omniIOR & ,
    					  const char * ,
					  const char * operation_id,
					  IOP::ServiceContextList & svclist )
{
    if ( !strcmp( operation_id, "shutdown" ) )
    {
	struct passwd * psw = getpwuid( geteuid() );
	if ( psw == 0 )
	{
	    ers::error( daq::ipc::Exception( ERS_HERE, "Insufficient memory to allocate passwd structure" ) );
	}
        else
        {
	    CORBA::ULong len = svclist.length() + 1;
	    svclist.length(len);
	    svclist[len-1].context_id = IPC_SECURE_SHUTDOWN;
	    svclist[len-1].context_data.replace( strlen(psw->pw_name)+1,
						 strlen(psw->pw_name)+1,
						 (CORBA::Octet *)psw->pw_name,
						 0 );
	}
    }
}

extern "C" void * create_ipcclicp()
{
    return new IPCClientDefaultInterceptor;
}
