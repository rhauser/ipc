#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include <ers/ers.h>

#include <ipc/interceptors.h>
#include <ipc/exceptions.h>

#define IPC_SECURE_SHUTDOWN 0x1001

class IPCServerDefaultInterceptor : public IPCServerInterceptor
{
    void receiveRequest( const char * interface_id, const char * operation_id, IOP::ServiceContextList & );
};

void 
IPCServerDefaultInterceptor::receiveRequest(	const char * , 
						const char * operation_id, 
						IOP::ServiceContextList & svclist )
{
    if ( !strcmp( operation_id, "shutdown" ) )
    {
	////////////////////////////////////////////////
	// Get the user name for the server process
	////////////////////////////////////////////////
	struct passwd * psw = getpwuid( geteuid() );
	if ( psw == 0 )
	{
	    ers::error( daq::ipc::Exception( ERS_HERE, "Insufficient memory to allocate passwd structure" ) );
            return;
	}
		
	///////////////////////////////////////////////
	// Get the user name from the client's request
	// and compare it with the server's owner
	///////////////////////////////////////////////
	CORBA::ULong total = svclist.length();
	for (CORBA::ULong index = 0; index < total; index++)
        {
	    if (svclist[index].context_id == IPC_SECURE_SHUTDOWN)
	    {
		if ( strcmp( (const char*)(svclist[index].context_data.get_buffer()), psw->pw_name ) )
		{
		    throw CORBA::NO_PERMISSION();
		}
		break;
	    }
	}
    }
}

extern "C" void * create_ipcsrvicp()
{
    return new IPCServerDefaultInterceptor;
}
