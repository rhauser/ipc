////////////////////////////////////////////////////////////////////////////
//
//     servant.cc
//
//     source file for IPC library
//
//     Sergei Kolos June 2003
//
//    description:
//       This file contains implementation for the IPCServant methods
//
////////////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>

#include <chrono>

#include <boost/lexical_cast.hpp>

#include <ers/ers.h>
#include <ers/Configuration.h>

#include <ipc/partition.h>
#include <ipc/servant.h>
#include <ipc/util.h>

using std::chrono::system_clock;

IPCServant::IPCServant( const IPCPartition & partition, const std::string & name )
  : m_app_context(new ipc::servant::ApplicationContext),
    m_partition_context(new ipc::servant::PartitionContext)
{   
    m_app_context->time = system_clock::to_time_t(system_clock::now());
    m_app_context->pid = getpid();
    
    char buffer[256];
    
    gethostname( buffer, sizeof(buffer) );   
    m_app_context->host = CORBA::string_dup( buffer );
    
    struct passwd pswd;
    struct passwd * result = 0;
    char buff[1024];
    int error = getpwuid_r(geteuid(), &pswd, buff, 1024, &result);
    if ( error || !result )
    {
	ERS_LOG( "Can't get user name, error number = " << error );
	m_app_context->owner = CORBA::string_dup( "Unknown" );
    }
    else
   	m_app_context->owner = CORBA::string_dup( result->pw_name );

    const char * appname = ::getenv("TDAQ_APPLICATION_NAME");
    m_app_context->name = CORBA::string_dup( appname ? appname : "Unknown" );

    m_partition_context->partition = CORBA::string_dup( partition.name().c_str() );
    m_partition_context->name = CORBA::string_dup( name.c_str() );
}

ipc::servant::ApplicationContext *
IPCServant::app_context ( )
{
    m_app_context->debug_level = ers::Configuration::instance().debug_level();
    m_app_context->verbosity_level = ers::Configuration::instance().verbosity_level();
    ipc::servant::ApplicationContext_var i = m_app_context;
    return i._retn();
}

ipc::servant::PartitionContext *
IPCServant::partition_context ( )
{
    ipc::servant::PartitionContext_var i = m_partition_context;
    return i._retn();
}

void
IPCServant::ping()
{
    return ;
}

void
IPCServant::shutdown()
{
    return ;
}

void
IPCServant::test(const char *)
{
    return ;
}

CORBA::Short
IPCServant::debug_level()
{
    return ers::Configuration::instance().debug_level();
}

void
IPCServant::debug_level( CORBA::Short new_level )
{
    ers::Configuration::instance().debug_level( new_level );
}

CORBA::Short
IPCServant::verbosity_level()
{
    return ers::Configuration::instance().verbosity_level();
}

void
IPCServant::verbosity_level( CORBA::Short new_level )
{
    ers::Configuration::instance().verbosity_level( new_level );
}
