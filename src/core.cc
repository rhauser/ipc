#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pwd.h>

#include <iostream>
#include <sstream>
#include <memory>

#include <ers/ers.h>

#include <omniORB4/CORBA.h>
#include <omniORB4/internal/orbOptions.h>

#include <ipc/singleton.h>
#include <ipc/core.h>
#include <ipc/util.h>
#include <ipc/cache.h>

using namespace std;

namespace ipc
{
    extern void initialize_interceptors();
    extern void destroy_interceptors();
    extern void initialize_ssl_context();
}

bool			IPCCore::s_at_exit_registered = false;
bool                    IPCCore::s_bidir_used = false;
boost::mutex *		IPCCore::s_guard = new boost::mutex();
CORBA::ORB_ptr		IPCCore::s_orb_instance;
PortableServer::POA_ptr	IPCCore::s_root_poa;

//////////////////////////////////////////////////////
// This namepase contains some helper functions
//////////////////////////////////////////////////////

namespace
{
  string
  ior_to_corbaloc( const char * iorstr )
  {
    IOP::IOR ior;
    char hex[3] = { 0, 0, 0 };
    const char * ptr = iorstr + 4;
    
    cdrMemoryStream buf(strlen(ptr)/2,0);
    while( *ptr )
    {
	hex[0] = *ptr++;
	hex[1] = *ptr++;
	unsigned int v;
	sscanf( hex, "%x", &v );
	buf.marshalOctet((CORBA::Octet)v);
    }
    buf.rewindInputPtr();
    CORBA::Boolean b = buf.unmarshalBoolean();
    buf.setByteSwapFlag(b);

    ior.type_id = IOP::IOR::unmarshaltype_id(buf);
    ior.profiles <<= buf;
    
    string address = "corbaloc:iiop:";
    for (unsigned long count=0; count < ior.profiles.length(); count++)
    {
	if (ior.profiles[count].tag == IOP::TAG_INTERNET_IOP)
	{
	    IIOP::ProfileBody pBody;
	    IIOP::unmarshalProfile(ior.profiles[count],pBody);
	  
	    address += (const char*) pBody.address.host;
	    address += ":";
	    ostringstream out;
	    out << pBody.address.port;
	    address += out.str();
	    
	    for ( unsigned int i = 0; i < pBody.components.length(); i++ )
	    {
		if (pBody.components[i].tag==IOP::TAG_ALTERNATE_IIOP_ADDRESS)
		{
		    cdrEncapsulationStream e( pBody.components[i].component_data.get_buffer(),
					      pBody.components[i].component_data.length(), 0 );
		    IIOP::Address v;
		    v.host = e.unmarshalRawString();
		    v.port <<= e;
		    address += ",iiop:";
		    address += (const char*) v.host;
		    address += ":";
		    ostringstream out;
		    out << v.port;
		    address += out.str();
		}
		else if (pBody.components[i].tag==IOP::TAG_OMNIORB_UNIX_TRANS)
		{
		    cdrEncapsulationStream e( pBody.components[i].component_data.get_buffer(),
					      pBody.components[i].component_data.length(), 0 );
		    CORBA::String_var host = e.unmarshalRawString();
		    CORBA::String_var filename = e.unmarshalRawString();
		    address += ",omniunix:";
		    address += (const char*)filename;
		    address += ":";
		}
	    }
	    
	    address += '/';
	    for ( unsigned int i = 0; i < pBody.object_key.length(); i++ )
	    {
		if ( ipc::util::isValidChar( pBody.object_key[i] ) )
	      	    address += (char)pBody.object_key[i];
		else
		{
		    char escape[4];
		    sprintf( escape, "%%%.2x", pBody.object_key[i] );
		    address.append( escape, 3 );
		}
	    }
	}
    }
    return address;
  }

  void 
  setup_file_descriptors()
  {
#ifdef RLIMIT_NOFILE            
    struct rlimit rlp;			// if there is a way to know
    getrlimit(RLIMIT_NOFILE,&rlp);	// how many sockets can be opened
    if ( rlp.rlim_cur < rlp.rlim_max )	// simultaneously per one process
    {					// limit can be changed in the shell 
	rlp.rlim_cur = rlp.rlim_max;	// so sometime rlim_cur > rlim_max
	setrlimit(RLIMIT_NOFILE,&rlp);
	getrlimit(RLIMIT_NOFILE,&rlp);	// get new number for crosscheck
    }        

    ERS_DEBUG( 2, "Maximum number of simultaneous connections is " << rlp.rlim_cur );        
#else
    ERS_DEBUG( 2, "Maximum number of simultaneous connections can not be increased" );        
#endif
  }

}

/*! This function removes CORBA specific arguments from the given array and
	put them to the std::list container.
    \param  argc number of command line arguments
    \param  argv an array which contains command line arguments
    \return list of parsed argumen:value pairs
    \throw  daq::ipc::UnknownOption 
    \throw  daq::ipc::BadOptionValue 
 */
list< pair< string, string > >
IPCCore::extractOptions( int & argc, char ** argv )
{
    list< pair< string, string > > opt;
    try
    {
	omni::orbOptions::singleton().reset();
    	omni::orbOptions::singleton().extractInitOptions(argc,argv);
    }
    catch (const omni::orbOptions::Unknown& ex)
    {
	throw daq::ipc::UnknownOption( ERS_HERE, (const char *)ex.key );
    }
    catch (const omni::orbOptions::BadParam& ex)
    {
	throw daq::ipc::BadOptionValue( ERS_HERE, (const char *)ex.key,
	        (const char *)ex.value, (const char *)ex.why );
    }

    omni::orbOptions::sequenceString_var newOpts;
    newOpts = omni::orbOptions::singleton().dumpSpecified();
    
    for (CORBA::ULong i = 0; i < newOpts->length(); i++)
    {
	string option = (const char*)newOpts[i];
	string::size_type pos = option.find( '=' );
	
	string key = option.substr(0,pos);
	key.erase( key.find_last_not_of( ' ' ) + 1 );
	string val;
	if ( pos != string::npos )
	{
	    val = option.substr(pos+1);
	    val.erase( 0, val.find_first_not_of( ' ' ) );
	}
	opt.push_back( make_pair( key, val ) );
    }
    return opt;
}

/*! This function initializes CORBA broker. It must be called at least once by any
	application which is using IPC before it tries to utilise any IPC function.
    \param  opt list of CORBA specific parameters
    \throw  daq::ipc::AlreadyInitialized 
    \throw  daq::ipc::CannotInitialize 
    \sa IPCCore::init( int & argc, char ** argv )
 */
void 
IPCCore::init( const std::list< std::pair< std::string, std::string > > & opt )
{   
    {
	boost::mutex::scoped_lock ml( *s_guard );

	if ( !CORBA::is_nil(s_orb_instance) && !CORBA::is_nil(s_root_poa) )
	{
	    throw daq::ipc::AlreadyInitialized( ERS_HERE );
	}


        s_bidir_used = getenv("TDAQ_IPC_USE_BIDIR") != 0;

	///////////////////////////////////
	// check and set the timeout value
	///////////////////////////////////
	string timeout = "10000";
	const char * env = getenv( "TDAQ_IPC_TIMEOUT" );
	if ( env != 0 )
	{
	    istringstream in( env );
	    long val = 0;
	    in >> val;
	    if ( val < 1000 )
	    {
		throw daq::ipc::BadOptionValue( ERS_HERE,
		        "Timeout", env, "It must be more then 1000 milliseconds." );
	    }
	    timeout = env;
	}

	const char* (*options)[2] = new const char*[opt.size()+10][2];
	int idx = 0;
	options[idx][0] = "clientCallTimeOutPeriod";
	options[idx][1] = timeout.c_str();
	idx++;
	///////////////////////////////////
	// set the default parameters
	///////////////////////////////////
	options[idx][0] = "verifyObjectExistsAndType";
	options[idx][1] = "0";
	idx++;

	if (s_bidir_used) {
            options[idx][0] = "offerBiDirectionalGIOP";
            options[idx][1] = "1";
            idx++;
            options[idx][0] = "acceptBiDirectionalGIOP";
            options[idx][1] = "1";
            idx++;
            options[idx][0] = "clientTransportRule";
            options[idx][1] = "* unix,tcp,ssl,bidir";
            idx++;
            options[idx][0] = "serverTransportRule";
            options[idx][1] = "* unix,tcp,ssl,bidir";
            idx++;
            options[idx][0] = "supportCurrent";
            options[idx][1] = "1";
            idx++;
	} else {
            options[idx][0] = "clientTransportRule";
            options[idx][1] = "* unix,tcp,ssl,bidir";
            idx++;
            options[idx][0] = "serverTransportRule";
            options[idx][1] = "* unix,tcp,ssl,bidir";
            idx++;
            options[idx][0] = "supportCurrent";
            options[idx][1] = "0";
            idx++;
	}

	for( list< pair< string, string > >::const_iterator it = opt.begin(); it != opt.end(); ++idx, ++it )
	{
	    options[idx][0] = (*it).first.c_str();
	    options[idx][1] = (*it).second.c_str();
	}
	options[idx][0] = 0;
	options[idx][1] = 0;

    // This sets up a custom SSL context for IPC.
    // It must be called before ORB_init().
    //
    // This feature is enabled by setting
    // the environment variable TDAQ_IPC_ENABLE_TLS=1
    // In all other cases the feature is disabled.
    ipc::initialize_ssl_context();

	try
	{
	    int argc = 0;
	    s_orb_instance = CORBA::ORB_init( argc, 0, "omniORB4", options );
	    CORBA::Object_var obj = s_orb_instance -> resolve_initial_references("RootPOA");
	    s_root_poa = PortableServer::POA::_narrow(obj);
	    PortableServer::POAManager_var poa_man = s_root_poa->the_POAManager();

	    ///////////////////////////////////////////////////////////////////////
	    // Force creation of the cache singleton
	    // It will be created before any IPC object
	    // and therefore destroyed after the last of them
	    ///////////////////////////////////////////////////////////////////////
	    IPCSingleton<IPCCache>::instance();

	    ///////////////////////////////////////////////////////////////////////
	    // Activate the Root POA
	    ///////////////////////////////////////////////////////////////////////
	    poa_man->activate();

	    delete[] options;

	}
	catch( CORBA::SystemException & ex )
	{
	    delete[] options;
	    throw daq::ipc::CannotInitialize( ERS_HERE, daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
	}
    }
    
    ///////////////////////////////////////////////////////////////////////
    // This call increases a number of sockets which can be opened by
    // the current program to the maximum
    ///////////////////////////////////////////////////////////////////////
    setup_file_descriptors();
    
    ///////////////////////////////////////////////////////////////////////
    // This call instantiates interceptors singleton
    // This must not be protected with the IPCCore internal mutex
    // since it may also results in calling one of the IPCCore functions
    ///////////////////////////////////////////////////////////////////////
    ipc::initialize_interceptors();

    ///////////////////////////////////////////////////////////////////////
    // This function will be called at exit from an application and
    // will perform the ORB shutdown. It will be the first one called
    // by this mechanism, thus all other functions registered with atexit
    // must not use the ORB object
    ///////////////////////////////////////////////////////////////////////
    if ( !s_at_exit_registered )
    {
	s_at_exit_registered = true;
	atexit( atExitFun );
    }
}

/*! This function initializes CORBA broker. It must be called at least once by any
	application which is using IPC before it tries to utilise any IPC function.
	This function removes CORBA specific arguments from the given array.
    \param  argc number of command line arguments
    \param  argv an array which contains command line arguments
    \param  opt list of CORBA specific parameters
    \throw  daq::ipc::AlreadyInitialized 
    \throw  daq::ipc::CannotInitialize 
    \sa IPCCore::init( const std::list< std::pair< std::string, std::string > > & opt )
 */
void 
IPCCore::init( int & argc, char ** argv )
{    
    list< pair< string, string > > opt;
    try
    {
	opt = extractOptions( argc, argv );
    }
    catch( daq::ipc::Exception & ex )
    {
    	throw daq::ipc::CannotInitialize( ERS_HERE, ex );
    }
    init( opt );
}

void 
IPCCore::shutdown( )
{
    boost::mutex::scoped_lock ml( *s_guard );
    
    ERS_DEBUG( 1, "shutdown is called" );
    
    if ( CORBA::is_nil(s_orb_instance) || CORBA::is_nil(s_root_poa) )
    {
	throw daq::ipc::NotInitialized( ERS_HERE );
    }

    // Clear object references because they will become invalid after the ORB shutdown
    IPCSingleton<IPCCache>::instance().clear();

    try
    {
	// Sometimes deactivating Root POA causes hang up of the ORB shutdown
        // which probably indicates a bug in the omniORB
	//PortableServer::POAManager_var poa_man = s_root_poa->the_POAManager();
        //poa_man -> deactivate( true, true );

        s_orb_instance->shutdown( true );
	s_orb_instance->destroy();
        CORBA::release( s_orb_instance );
	s_orb_instance = CORBA::ORB::_nil();
    }
    catch(CORBA::SystemException & ex)
    {
	throw daq::ipc::CorbaSystemException( ERS_HERE, &ex );
    }

    ipc::destroy_interceptors();
}

bool
IPCCore::isInitialised() throw()
{
    boost::mutex::scoped_lock ml( *s_guard );
    
    return ( !CORBA::is_nil(s_orb_instance) && !CORBA::is_nil(s_root_poa) );
}

bool
IPCCore::isBidirUsed() throw()
{
    boost::mutex::scoped_lock ml( *s_guard );

    return s_bidir_used;
}

CORBA::ORB_var 
IPCCore::getORB ()
{
    boost::mutex::scoped_lock ml( *s_guard );
    
    if ( CORBA::is_nil(s_orb_instance) )
    {
	throw daq::ipc::NotInitialized( ERS_HERE );
    }
    return CORBA::ORB::_duplicate( s_orb_instance );
}

PortableServer::POA_var
IPCCore::getRootPOA ()
{
    boost::mutex::scoped_lock ml( *s_guard );
    
    if ( CORBA::is_nil(s_root_poa) )
    {
	throw daq::ipc::NotInitialized( ERS_HERE );
    }
    return PortableServer::POA::_duplicate( s_root_poa );
}

void
IPCCore::atExitFun()
{
    try
    {
    	shutdown();
    }
    catch( daq::ipc::Exception & ex )
    {
    	ers::debug( ex );
    }
}

string
IPCCore::objectToString( CORBA::Object_ptr obj, ObjectRefType type )
{
    boost::mutex::scoped_lock ml( *s_guard );
    
    string address;
    if ( CORBA::is_nil(s_orb_instance) )
    {
	throw daq::ipc::NotInitialized( ERS_HERE );
    }
    
    try
    {
	CORBA::String_var ior = s_orb_instance -> object_to_string( obj );
	if ( (const char *)ior != 0 )
        {
	    address = (const char *)ior;
        }
    }
    catch(CORBA::SystemException & ex)
    {
	throw daq::ipc::CorbaSystemException( ERS_HERE, &ex );
    }
    
    if ( type == Corbaloc )
    {
	return ior_to_corbaloc( address.c_str() );
    }
    
    return address;
}

CORBA::Object_ptr
IPCCore::stringToObject( const string & ref )
{    
    boost::mutex::scoped_lock ml( *s_guard );
    
    if ( CORBA::is_nil(s_orb_instance) )
    {
	throw daq::ipc::NotInitialized( ERS_HERE );
    }
    
    try
    {
	return s_orb_instance -> string_to_object( ref.c_str() );
    }
    catch(CORBA::SystemException & ex)
    {
	throw daq::ipc::CorbaSystemException( ERS_HERE, &ex );
    }
}
