/*
 * backup.h
 *
 *  Created on: Oct 19, 2015
 *      Author: kolos
 */

#ifndef IPC_SERVER_BACKUP_H_
#define IPC_SERVER_BACKUP_H_

#include <string>
#include <fstream>

#include <ipc/server/CosNaming_impl.h>

class bofstream : public std::ofstream
{
public:
    bofstream(const std::string & fname);

    ~bofstream();

private:
    std::string m_name;
    std::string m_bck_name;
};

class bifstream : public std::ifstream
{
public:
    bifstream(const std::string & fname);

private:
    std::string m_name;
    std::string m_bck_name;
};

namespace CosNaming
{
    std::ostream& operator<<=( std::ostream & , const std::string & );
    std::istream& operator>>=( std::istream & , std::string & );
    std::ostream& operator<<=( std::ostream & , const CosNaming::String & );
    std::istream& operator>>=( std::istream & , CosNaming::String & );
    std::ostream& operator<<=( std::ostream & , const ipc::URI & );
    std::istream& operator>>=( std::istream & , ipc::URI & );
    std::ostream& operator<<=( std::ostream & , const CosNaming::Type & );
    std::istream& operator>>=( std::istream & , CosNaming::Type & );
    std::ostream& operator<<=( std::ostream & , const CosNaming::BindingType & );
    std::istream& operator>>=( std::istream & , CosNaming::BindingType & );
    std::ostream& operator<<=( std::ostream & , const CosNaming::Name & );
    std::istream& operator>>=( std::istream & , CosNaming::Name & );
    std::ostream& operator<<=( std::ostream & , const CosNaming::NameComponent & );
    std::istream& operator>>=( std::istream & , CosNaming::NameComponent & );
}

#endif /* IPC_SERVER_BACKUP_H_ */
