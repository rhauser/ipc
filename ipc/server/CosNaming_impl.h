#ifndef COS_NAMING_IMPL_H
#define COS_NAMING_IMPL_H

////////////////////////////////////////////////////////////////////////////
//      CosNaming_impl.h
//
//      header file for the IPC library
//
//      Sergei Kolos December 1999
//
//      description:
//              This file contains implementation for the CORBA Naming service
//
////////////////////////////////////////////////////////////////////////////

#include <partition/partition.hh>

#include <string>
#include <unordered_map>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

#include <ers/ers.h>

#include <ipc/server/String.h>

/*! \class ipc::InvalidBackupFile
 *  This issue is thrown if the restore operation is applyed to the file
 *  which is corrupted or is not a valid IPC server backup file.
 */
ERS_DECLARE_ISSUE( CosNaming,
		   InvalidBackupFile,
		   reason,
		  ((std::string)reason )
		 )

namespace CosNaming
{
    enum Type { LocalContext, RemoteContext, ObjectReference };
    
    class NamingContext_impl;

    class Element : boost::noncopyable
    {
	NamingContext_var m_nc;
	mutable CORBA::Object_var m_obj;
	NamingContext_impl * m_nc_impl;
        mutable ipc::URI m_uri;
        
      public:

	Element( )
	  : m_nc_impl( 0 )
	{ ; }

	Element( NamingContext_ptr nc, NamingContext_impl * nc_impl = 0 )
	  : m_nc( NamingContext::_duplicate( nc ) ),
	    m_nc_impl( nc_impl )
	{ ; }

	Element( CORBA::Object_ptr obj )
	  : m_obj( CORBA::Object::_duplicate( obj ) ),
            m_nc_impl( 0 )
	{ ; }

	Element( ipc::URI & uri )
	  : m_nc_impl( 0 )
	{ 
            unsigned int l = uri.length();
            m_uri.replace( l, l, uri.get_buffer( true ), true );
        }

	ipc::URI * ref_dup();
        
	const ipc::URI & ref()
        { return m_uri; }
        
        bool is_obj() const {
	    return ( CORBA::is_nil(m_nc) );
	}

	bool is_nc () const {
	    return ( !CORBA::is_nil(m_nc) );
	}

	NamingContext_ptr nc() const {
	    return ( m_nc );
	}

	CORBA::Object_ptr obj() const;
        
	NamingContext_impl * nc_impl() const {
	    return ( m_nc_impl );
	}

        bool empty() {
	    return ( CORBA::is_nil(m_nc) && CORBA::is_nil(m_obj) );
        }
	
        CORBA::Object_ptr nc_or_obj() const {
	    return ( is_obj() ? obj() : nc() );
	}
        
        void write( std::ostream & out ) const;
    };
        
    class NamingContext_impl :	public virtual POA_ipc::NamingContextOpt,
				public virtual PortableServer::RefCountServantBase
    {
	friend class BindingIterator_impl;

      public:
	NamingContext_impl( )
        { ; }
        
	~NamingContext_impl();
        
      protected:
        void read( std::istream & in );
        
        void write( std::ostream & out ) const;

      protected:
	static PortableServer::POA_ptr	the_poa;
        static unsigned int the_global_request_counter;
        static unsigned int the_modifying_request_counter;        

      private:
	void bind(const Name & n, CORBA::Object_ptr obj);

	void rebind(const Name & n, CORBA::Object_ptr obj);

	void bind_context(const Name & n, NamingContext_ptr nc);

	void rebind_context(const Name & n, NamingContext_ptr nc);

	CORBA::Object_ptr resolve(const Name & n);

	void unbind(const Name & n);

	NamingContext_impl * createNewContext( ) const;
        
	NamingContext_ptr new_context();

	NamingContext_ptr bind_new_context(const Name & n);

	void destroy();

	void list(CORBA::ULong how_many, BindingList_out bl, BindingIterator_out bi);

	void associate(const Name & n, const ipc::URI & obj);
        
	void obtain(const Name & n, ipc::URI_out obj);
        
	void doBind( Name & name, const boost::shared_ptr<Element> & elem, bool rebind );
        
      private:
	typedef std::unordered_map<String, boost::shared_ptr<Element>, FNV_hash> BindingMap;

	mutable boost::mutex	m_mutex;
        BindingMap		m_bindings;
    };

    class BindingIterator_impl : public POA_CosNaming::BindingIterator,
				 public PortableServer::RefCountServantBase
    {
	BindingList_var	m_bl;
	unsigned long	m_idx;

      public:

	BindingIterator_impl( BindingList_var & bl );

	virtual bool next_one( Binding_out b );
	virtual bool next_n( CORBA::ULong how_many, BindingList_out bl );

	virtual void destroy( );
    };    
}

#endif
