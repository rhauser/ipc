/*
 * String.h
 *
 *  Created on: May 6, 2013
 *      Author: Serguei Kolos
 */

#ifndef IPC_SERVER_STRING_H_
#define IPC_SERVER_STRING_H_

namespace CosNaming
{
    class String
    {
    public:
	String()
		: m_data(""), m_owner(false)
	{
	    ;
	}

	String(const char * data)
		: m_data(data), m_owner(false)
	{
	    ;
	}

	String(char * data)
		: m_data(data), m_owner(true)
	{
	    ;
	}

	~String()
	{
	    if (m_owner)
		delete[] m_data;
	}

	String(const String & s)
		: m_data(s.m_data), m_owner(s.m_owner)
	{
	    s.m_owner = false;
	}

	String & operator=(const String & s)
	{
	    if (this != &s) {
		m_data = s.m_data;
		m_owner = s.m_owner;
		s.m_owner = false;
	    }
	    return *this;
	}

	const char * c_str() const
	{
	    return m_data;
	}

	unsigned int size() const
	{
	    return strlen(m_data);
	}

	bool operator==(const String & s) const
	{
	    return !strcmp(m_data, s.m_data);
	}

	bool operator<(const String & s) const
	{
	    return (strcmp(m_data, s.m_data) < 0);
	}

    private:
	const char * m_data;
	mutable bool m_owner;
    };

    template <int >
    struct FNV_primes;

    template <>
    struct FNV_primes<4>
    {
	static const size_t offset = 2166136261;
	static const size_t prime  = 16777619;
    };

    template <>
    struct FNV_primes<8>
    {
	static const size_t offset = 14695981039346656037UL;
	static const size_t prime  = 1099511628211UL;
    };

    struct FNV_hash
    {
	size_t operator() ( const String & s ) const
	{
	    size_t h = FNV_primes<sizeof(size_t)>::offset;
	    for (const char * p = s.c_str(); *p != 0; ++p) {
		h = h^(*p)*FNV_primes<sizeof(size_t)>::prime;
	    }
	    return h;
	}
    };
}

#endif /* STRING_H_ */
