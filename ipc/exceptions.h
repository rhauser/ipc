/*
 *  exceptions.h
 *  ipc
 *  Defines IPC issues. 
 *  Modified by Serguei Kolos on 24.08.05.
 *  Copyright 2005 CERN. All rights reserved.
 *
 */
#ifndef IPC_EXCEPTIONS_H
#define IPC_EXCEPTIONS_H

#include <iostream>
#include <string>

#include <omniORB4/CORBA.h>

#include <ers/ers.h>

namespace CORBA {
  std::ostream & operator<< ( std::ostream & out, const SystemException * ex );
  std::istream & operator>> ( std::istream & in, SystemException * & ex );
}
std::ostream & operator<< ( std::ostream & out, const CORBA::SystemException & ex );

#define IPC_ASSERT( x )	ERS_ASSERT_MSG( x, "of inconsistent internal state of the IPC library.\n" \
					   "This indicates a bug in the application using IPC or maybe in IPC itself." )

#define IPC_ABORT( ex )	ERS_ASSERT_MSG( 0, ex << " This indicates a bug in the application using IPC or maybe in IPC itself." )

/*! \namespace daq
 *  This is a wrapping namespace for all IPC exceptions.
 */ 
namespace daq {

    /*! \namespace daq::ipc
     *	This is a wrapping namespace for all IPC exceptions and internal utilities.
     */

    /*! \class ipc::Exception
     *	This is a base class for all IPC exceptions.
     */
    ERS_DECLARE_ISSUE( ipc, Exception, ERS_EMPTY, ERS_EMPTY )

    /*! \class ipc::CorbaSystemException
     *	This is a wrapper for CORBA exceptions.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    CorbaSystemException,
			    ipc::Exception,
			    "CORBA system exception \"" << exception << "\" has been raised",
			    ERS_EMPTY,
			    ((CORBA::SystemException *)exception )
		     	)

    /*! \class ipc::UserException
     *	This is a wrapper for the interface specific CORBA exceptions.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    CorbaUserException,
			    ipc::Exception,
			    "CORBA user exception \"" << name << "\" has been raised",
			    ERS_EMPTY,
			    ((const char *)name )
		     	)

    /*! \class ipc::FileException
     *	Base class for any file related exception.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    FileException,
			    ipc::Exception,
			    ERS_EMPTY,
                            ERS_EMPTY,
			    ((std::string)file_name )
		     	)

    /*! \class ipc::CannotCreateDirectory
     *	This issue is reported when a certain file can not be opened by any reason.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    CannotCreateDirectory,
			    ipc::FileException,
			    "Can not create \"" << directory_name << "\" directory because " << reason,
			    ((std::string)directory_name ),
			    ((std::string)reason )
			  )

    /*! \class ipc::CannotOpenFile
     *	This issue is reported when a certain file can not be opened by any reason.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    CannotOpenFile,
			    ipc::FileException,
			    "Can not open \"" << file_name << "\" file because " << reason,
			    ((std::string)file_name ),
			    ((std::string)reason )
			  )

    /*! \class ipc::CannotReadFile
     *	This issue indicates error while trying to read from the file.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    CannotReadFile,
			    ipc::FileException,
			    "Can not read from the \"" << file_name << "\" file because " << reason,
			    ((std::string)file_name ),
			    ((std::string)reason )
			  )

    /*! \class ipc::CannotWriteFile
     *	This issue indicates error while trying to write to the file.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    CannotWriteFile,
			    ipc::FileException,
			    "Can not write to the \"" << file_name << "\" file because " << reason,
			    ((std::string)file_name ),
			    ((std::string)reason )
			  )

    /*! \class ipc::InvalidInitialReference
     *	This issue is reported if an initial IPC reference is invalid.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    NotFileReference,
			    ipc::Exception,
			    "Object reference \"" << reference << "\" is not a file",
                            ERS_EMPTY,
			    ((std::string)reference )
		     )

    /*! \class ipc::BadReferenceFormat
     *	This issue is reported if an initial IPC reference is badly formatted.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    BadReferenceFormat,
			    ipc::Exception,
			    "Initial reference \"" << reference << "\" has invalid format",
                            ERS_EMPTY,
			    ((std::string)reference )
		     )

    /*! \class ipc::InvalidObjectName
     *	This issue is reported if an IPC object name is invalid, i.e. empty.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    InvalidObjectName,
			    ipc::Exception,
			    "Empty string is provided as the name of IPC object",
                            ERS_EMPTY,
                            ERS_EMPTY
		     )

    /*! \class ipc::UnknownOption
     *	This issue indicates that unrecognized option is given to a function.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    UnknownOption,
			    ipc::Exception,
			    "Unknown option \"" << option_name << "\" is given ",
                            ERS_EMPTY,
			    ((std::string)option_name )
		      )

    /*! \class ipc::BadOptionValue
     *	This issue indicates that bad value is given to one of the options.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    BadOptionValue,
			    ipc::Exception,
			    "Bad value \"" << option_value << "\" is given to the \""
					   << option_name << "\" option \" (" << reason << ")",
                            ERS_EMPTY,
			    ((std::string)option_name )
			    ((std::string)option_value )
			    ((std::string)reason )
		      )

    /*! \class ipc::InvalidPartition
     *	Partiton IPC server is unavailable, i.e. either it is not
     *	    running or there are problems with network connections.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    InvalidPartition,
			    ipc::Exception,
			    "Partition \"" << name << "\" does not exist",
                            ERS_EMPTY,
			    ((std::string)name )
		     )

    /*! \class ipc::ObjectNotFound
     *	This issue is reported if object is not registered with the given IPC partition.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    ObjectNotFound,
			    ipc::Exception,
			    "The object \"" << name << "\" of the \"" << type << "\" type is not published in the \""
					    << partition << "\" partition ",
			    ERS_EMPTY,
                            ((std::string)partition )
			    ((std::string)type )
			    ((std::string)name )
		     )

    /*! \class ipc::PluginException
     *	This issue is reported if dynamic library can not be loaded.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    PluginException,
			    ipc::Exception,
			    "Loading of the \"" << plugin_name << "\" plugin failed because \"" << reason << "\"",
			    ERS_EMPTY,
                            ((std::string)plugin_name )
			    ((std::string)reason )
		     )

    /*! \class ipc::IgnorePluginException
     *	This issue is reported if dynamic library can not be loaded.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    IgnorePluginException,
			    ipc::Exception,
			    "The \"" << plugin_name << "\" plugin will not be loaded because \"" << reason << "\"",
			    ERS_EMPTY,
                            ((std::string)plugin_name )
			    ((std::string)reason )
		     )

    /*! \class ipc::AlreadyInitialized
     *	This issue is reported if one tryes to initialize the IPCCore more then once.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    AlreadyInitialized,
			    ipc::Exception,
			    "IPCCore has been already initialized",
                            ERS_EMPTY,
                            ERS_EMPTY
		     )

    /*! \class ipc::NotInitialized
     *	This issue is reported if one tryes to shutdown the IPCCore
     * which was not yet initialized.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    NotInitialized,
			    ipc::Exception,
			    "IPCCore has not been yet initialized",
                            ERS_EMPTY,
                            ERS_EMPTY
		     )

    /*! \class ipc::CannotInitialize
     *	This issue is reported if an attempt of initializing IPCCore failed.
     */
    ERS_DECLARE_ISSUE_BASE( ipc,
			    CannotInitialize,
			    ipc::Exception,
			    "IPCCore initialization failed",
                            ERS_EMPTY,
                            ERS_EMPTY
		     )
}

#endif
