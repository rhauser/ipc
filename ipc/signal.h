/*
 * signal.h
 *
 *  Created on: Nov 5, 2020
 *      Author: kolos
 */

#ifndef _IPC_SIGNAL_H_
#define _IPC_SIGNAL_H_

#include <csignal>
#include <cstdint>
#include <vector>

namespace daq
{
    namespace ipc
    {
        /*! \brief This class can be used for blocking a thread until a signal is received.
         *  \ingroup public
         */
        class signal
        {
        public:
            static int wait_for(
                    std::vector<int> const& signums = {SIGINT, SIGTERM},
                    uint32_t precision_ms = 1000) noexcept;

            static int raise(int signum = SIGTERM) {
                return ::raise(signum);
            }

        private:
            static void signalHandler(int sig);

        private:
            static volatile std::sig_atomic_t m_signal;
        };
    }
}

#endif /* _IPC_SIGNAL_H_ */

