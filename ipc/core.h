#ifndef IPC_CORE_H
#define IPC_CORE_H

////////////////////////////////////////////////////////////////////////////
//
//      core.h
//
//      header file for the IPC library
//
//      Sergei Kolos June 2003
//
//      description:
//              IPCCore - initialises the ORB, resolves the Root POA
//			  and set up the necessary interceptors
//
////////////////////////////////////////////////////////////////////////////

#include <omniORB4/CORBA.h>
#include <string>
#include <list>

#include <boost/thread/mutex.hpp>

#include <ipc/exceptions.h>

/*! \file core.h Defines the IPCCore class which provides the basic IPC initialization 
 * functions and utilities. 
 * \author Serguei Kolos
 * \version 1.0 
 */

/*! This class provides the basic IPC initialization functions.
    \ingroup
    \sa
 */
class IPCCore
{
  public:
    
    /*! \enum Defines possible formats for CORBA object references.
	\sa IPCCore::objectToString( CORBA::Object_ptr obj, ObjectRefType type = Ior )
     */
    enum ObjectRefType { Ior, /*!< IOR format */
    			 Corbaloc /*!< CORBALOC format */ };
    
    static void init( int & argc, char ** argv );
    
    static void init( const std::list< std::pair < std::string, std::string > > & options );

    static std::list< std::pair < std::string, std::string > > 
    		extractOptions( int & argc, char ** argv );
		    
    static void shutdown();
    
    static bool isInitialised() throw();
    
    static bool isBidirUsed() throw();

    static CORBA::ORB_var getORB ();
    
    static PortableServer::POA_var getRootPOA ();
    
    static std::string objectToString( CORBA::Object_ptr obj, ObjectRefType type = Ior );
                                                
    static CORBA::Object_ptr stringToObject( const std::string & ref );
    
  private:    
    static void atExitFun();
    
    static bool				s_at_exit_registered;
    static bool                         s_bidir_used;
    static boost::mutex *		s_guard;
    static CORBA::ORB_ptr		s_orb_instance;
    static PortableServer::POA_ptr	s_root_poa;
};

#endif // IPC_CORE_H
