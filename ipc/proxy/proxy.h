////////////////////////////////////////////////////////////////////////////
//      proxy.h
//
//      Implementation of the IPC proxy application
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains declaration of the IPC proxy manager
//
////////////////////////////////////////////////////////////////////////////
#include <string>

#include <proxy/proxy.hh>
#include <ipc/object.h>
#include <ipc/servant.h>

class IPCProxy : public IPCObject<POA_ipc::proxy,ipc::multi_thread,ipc::persistent>,
		 public IPCServant
{    
  public:
  
    static bool exists();

  private:    
    CORBA::Object_ptr create( const char * interface, const char * ior );
    
    void get_supported_interfaces( ipc::proxy::list_out interfaces );
    
    std::string unique_id() const;
};

