////////////////////////////////////////////////////////////////////////////
//      factory.h
//
//      Implementation of the IPC proxy application
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains implementation of the IPC proxy factory
//
////////////////////////////////////////////////////////////////////////////

#ifndef IPC_PROXY_FACTORY_H
#define IPC_PROXY_FACTORY_H

#include <type_traits>

#include <ipc/object.h>
#include <ipc/proxy/factorybase.h>
#include <ipc/proxy/manager.h>
#include <ipc/proxy/object.h>
#include <ipc/proxy/namedobject.h>

template <class I, class POA_I, template <class > class POA_I_tie,
    bool = std::is_base_of_v<POA_ipc::servant, POA_I>>
class IPCProxyFactory;

template <class I, class POA_I, template <class > class POA_I_tie>
class IPCProxyFactory<I, POA_I, POA_I_tie, true> : public IPCProxyFactoryBase
{
    typedef IPCProxyObject<I,POA_I,POA_I_tie> Object;
    typedef IPCProxyNamedObject<I,POA_I,POA_I_tie> NamedObject;
    
  public:
    
    IPCProxyFactory( )
      : interface_( I::_PD_repoId )
    {
    	IPCSingleton<IPCProxyManager>::instance().registerFactory( this );
    }
            
    IPCProxyObjectBase * create( const std::string & ior ) override
    {
	return new Object( ior );
    }
    
    IPCProxyObjectBase * create( const IPCPartition & partition,
            const std::string & name, CORBA::Object_ptr remote ) override
    {
        return new NamedObject( partition, name, remote );
    }

    const std::string & interfaceId( ) override
    {
    	return interface_;
    };
    
  private:
    std::string interface_;
};

template <class I, class POA_I, template <class > class POA_I_tie>
class IPCProxyFactory<I, POA_I, POA_I_tie, false> : public IPCProxyFactoryBase
{
    typedef IPCProxyObject<I,POA_I,POA_I_tie> Object;

  public:

    IPCProxyFactory( )
      : interface_( I::_PD_repoId )
    {
        IPCSingleton<IPCProxyManager>::instance().registerFactory( this );
    }

    IPCProxyObjectBase * create( const std::string & ior ) override
    {
        return new Object( ior );
    }

    const std::string & interfaceId( ) override
    {
        return interface_;
    };

  private:
    std::string interface_;
};

#endif
