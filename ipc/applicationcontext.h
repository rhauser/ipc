#ifndef IPC_APPLICATION_CONTEXT_H
#define IPC_APPLICATION_CONTEXT_H

///////////////////////////////////////////////////////////////////////////
//      applicationcontext.h
//
//      header file for the IPC library
//
//      Sergei Kolos, June 2013
//
//      description:
//        IPCApplicationContext - helper class for dealing with remote object context
///////////////////////////////////////////////////////////////////////////
#include <string>

#include <chrono>

#include <ipc/ipc.hh>

using std::chrono::system_clock;

struct IPCApplicationContext : protected ipc::servant::ApplicationContext_var
{
    IPCApplicationContext( ipc::servant_ptr entity );
    IPCApplicationContext( POA_ipc::servant & entity );
    
    const bool				m_valid;
    const std::string			m_name;
    const std::string			m_owner;
    const std::string			m_host;
    const system_clock::time_point	m_time;
    const unsigned int			m_pid;
    const short				m_debug_level;
    const short				m_verbosity_level;

private:
    IPCApplicationContext(ipc::servant::ApplicationContext* p);
};

#endif
