#ifndef IPC_SERVANTBASE_H
#define IPC_SERVANTBASE_H

/*! \file servantbase.h Defines base class for IPC servants.
 * \author Serguei Kolos
 * \version 1.0 
 */

/*! \brief Base class for IPC CORBA servants. Both IPCObject and
 	IPCNamedObject indirectly inherit from it.
    \ingroup public
    \sa IPCObject 
    \sa IPCObject
    \sa IPCNamedObject 
 */
template <class T>
class IPCServantBase : public virtual T
{  
  public:
      
    virtual ~IPCServantBase ( )
    { ; }
        
    /*! This method provides the only valid way of destroying IPC objects. It must be called
    	instead of the delete operator for any object which inherits from IPCObject or IPCNamedObject.
        \param  wait_for_completion	This parameter specify whether the function has to return
        				immediately or it must wait before the operation will be completed.
                                        Note that completing the object destructor might be a long operation
                                        since it will wait for completion of all the external requests on that 
                                        object. Default is false.
     */
    virtual void _destroy( bool wait_for_completion = false ) = 0;
    
  protected:
    IPCServantBase ( )
    { ; }
};

#endif
