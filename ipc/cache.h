#ifndef IPC_CACHE_H
#define IPC_CACHE_H

#include <string>
#include <map>

#include <boost/function.hpp>
#include <boost/noncopyable.hpp>
#include <boost/thread/mutex.hpp>

#include <omniORB4/CORBA.h>

#include <ers/Issue.h>

template <class T> class IPCSingleton;

class IPCCache : public boost::noncopyable
{
    template <class> friend class IPCSingleton;
    friend class IPCCore;
    
    typedef boost::function<CORBA::Object_ptr ( )> Resolver;
    
    struct Entity
    {
	Entity( const Resolver & resolver,
                CORBA::Object_ptr obj = CORBA::Object::_nil() ) 
	  : m_resolver( resolver ),
            m_object( CORBA::Object::_duplicate( obj ) )
	{ ; }
		
	Entity( CORBA::Object_ptr obj = CORBA::Object::_nil() ) 
	  : m_object( CORBA::Object::_duplicate( obj ) )
	{ ; }
	
        Entity( const Entity & ent )
          : m_resolver( ent.m_resolver ),
	    m_object( ent.m_object )
	{ ; }
        
        Entity & operator=( const Entity & ent )
        {
	    boost::mutex::scoped_lock ml( m_mutex );
            if ( &ent != this )
            {
		m_resolver = ent.m_resolver;
		m_object = ent.m_object;
            }
            return *this;
        }
        
        CORBA::Object_ptr getObject();

        bool recover(bool is_timedout);
                        	
      private:      
        mutable boost::mutex	m_mutex;
        Resolver		m_resolver;
	CORBA::Object_var	m_object;
    };
    
  public:
    
    ~IPCCache( );
		
    CORBA::Object_ptr find ( const std::string & reference, const Resolver & resolver );
        
  private:        
    IPCCache( );
    
    void destroy() { delete this; }

    void clear();

    CORBA::Boolean handleException( IPCCache::Entity * entity, CORBA::ULong retries, bool is_timedout );

    static CORBA::Boolean global_transient_handler( void * cookie, CORBA::ULong retries, const CORBA::TRANSIENT & );
    static CORBA::Boolean commfailure_handler ( void * cookie, CORBA::ULong retries, const CORBA::COMM_FAILURE& );
    static CORBA::Boolean system_handler ( void * cookie, CORBA::ULong retries, const CORBA::SystemException& );
    static CORBA::Boolean transient_handler ( void * cookie, CORBA::ULong retries, const CORBA::TRANSIENT& );
    static CORBA::Boolean timeout_handler ( void * cookie, CORBA::ULong retries, const CORBA::TIMEOUT& );

  private:        
    boost::mutex			m_mutex;
    std::map<std::string, Entity>	m_map;
};

#endif // IPC_CACHE_H
