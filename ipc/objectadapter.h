#ifndef IPC_OBJECT_ADAPTER_H
#define IPC_OBJECT_ADAPTER_H

////////////////////////////////////////////////////////////////////////////
//    objectmanager.h
//
//    header file for the IPC library
//
//    Sergei Kolos June 2003
//
//    description:
//         IPCObjectAdapter - manages objects states
//
////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include <omniORB4/CORBA.h>

#include <ers/ers.h>

#include <ipc/core.h>
#include <ipc/util.h>
#include <ipc/policy.h>

std::ostream & operator<<(std::ostream &, const PortableServer::ObjectId_var &);

template<class, class, class > class IPCObjectBase;

class IPCObjectAdapter
{
public:
    template<class T, class TP, class PP>
    IPCObjectAdapter(IPCObjectBase<T, TP, PP> * object);

    operator PortableServer::POA::_ptr_type() const
    {
	return PortableServer::POA::_duplicate(m_poa);
    }

    void remove(PortableServer::ServantBase * servant) const;

    template<class T, class TP, class PP>
    void add(IPCObjectBase<T, TP, PP> * object) const
    {
	std::string id(object->unique_id());
	try {
	    PortableServer::ObjectId_var oid =
		    PortableServer::string_to_ObjectId(id.c_str());
	    m_poa->activate_object_with_id(oid.in(), object);
	}
	catch (CORBA::UserException & ex) {
	    IPC_ABORT( daq::ipc::CorbaUserException( ERS_HERE, ex._name() ));
	}
	catch (CORBA::SystemException & ex) {
	    IPC_ABORT( daq::ipc::CorbaSystemException( ERS_HERE, &ex ));
	}

	CORBA::String_var pid;
	ERS_DEBUG( 1,
		"Object '" << id << "' has been activated with the '" << ( pid = m_poa->the_name() ) << "' adapter");
    }

private:
    PortableServer::POA::_var_type m_poa;
};

template<class T, class TP, class PP>
IPCObjectAdapter::IPCObjectAdapter(IPCObjectBase<T, TP, PP> * object)
{
    CORBA::PolicyList pl(7);
    pl.length(6);

    PortableServer::POA_var root_poa = IPCCore::getRootPOA();
    pl[0] = root_poa->create_thread_policy(object->thread_policy());
    pl[1] = root_poa->create_lifespan_policy(object->lifespan_policy());
    pl[2] = root_poa->create_id_assignment_policy(PortableServer::USER_ID);
    pl[3] = root_poa->create_implicit_activation_policy(
	    PortableServer::NO_IMPLICIT_ACTIVATION);
    pl[4] = root_poa->create_servant_retention_policy(PortableServer::RETAIN);
    pl[5] = root_poa->create_id_uniqueness_policy(PortableServer::UNIQUE_ID);

    if (IPCCore::isBidirUsed()) {
        CORBA::ORB_var orb = IPCCore::getORB();
        CORBA::Any bidir;
        bidir <<= BiDirPolicy::BOTH;
        pl.length(7);
        pl[6] = orb->create_policy(BiDirPolicy::BIDIRECTIONAL_POLICY_TYPE, bidir);
    }

    std::string poa_id = ipc::util::getTypeName(object) + object->poa_id_suffix();

    try {
	m_poa = root_poa->create_POA(poa_id.c_str(),
		PortableServer::POAManager::_nil(), pl);
	PortableServer::POAManager_var m_poaman = m_poa->the_POAManager();
	m_poaman->activate();
	ERS_DEBUG( 1, "New object adapter '" << poa_id << "' created");
    }
    catch (PortableServer::POA::AdapterAlreadyExists & ex) {
	m_poa = root_poa->find_POA(poa_id.c_str(), false);
	ERS_DEBUG( 1, "Object adapter '" << poa_id << "' already exists");
    }
    catch (CORBA::UserException & ex) {
	IPC_ABORT( daq::ipc::CorbaUserException( ERS_HERE, ex._name() ));
    }
    catch (CORBA::SystemException & ex) {
	IPC_ABORT( daq::ipc::CorbaSystemException( ERS_HERE, &ex ));
    }

    for (CORBA::ULong i = 0; i != pl.length(); ++i) {
	pl[i]->destroy();
    }
}

#endif
