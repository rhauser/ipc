#ifndef IPC_SINGLETON_H
#define IPC_SINGLETON_H

////////////////////////////////////////////////////////////////////////////
//
//      singleton.h
//
//      header file for the IPC library
//
//      Sergei Kolos June 2003
//
//      description:
//              IPCSingleton
//
////////////////////////////////////////////////////////////////////////////

#include <boost/thread/mutex.hpp>

template <class T>
class IPCSingleton
{
  public:
    
    static T & instance();
    
  private:    
    static void atExitFun();
    
    static boost::mutex	s_guard;
    static T *		s_instance;
};

template <class T>
boost::mutex IPCSingleton<T>::s_guard;

template <class T>
T *	 IPCSingleton<T>::s_instance;

template <class T>
T &
IPCSingleton<T>::instance()
{
    if ( !s_instance )
    {
	boost::mutex::scoped_lock ml( s_guard );
	if ( !s_instance )
	{
	    s_instance = new T();
	    atexit( atExitFun );   // It will be destroyed at exit from the program
	}
    }
    return *s_instance;
}

template <class T>
void
IPCSingleton<T>::atExitFun()
{
    s_instance -> destroy();
    s_instance = 0;
}

#endif // IPC_SINGLETON_H
