import ipc.*;

class TestServant extends ipc.NamedObject<ipc.server>
		  implements serverOperations
{
    TestServant( Partition partition, String name )
    {
	super( partition, name );
    }

    public java.lang.String ping_s( java.lang.String in )
    {
	System.err.println( "ping_s is called " );
        return in;
    }

    public void ping_a( java.lang.String in )
    {
	System.err.println( "ping_a is called " );
	return ;
    }
}

