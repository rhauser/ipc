import ipc.*;
import org.omg.CosNaming.*;
import java.lang.Runnable;

public class Test
{
    public static void test_generic_iterators() throws InvalidPartitionException
    {
	test_generic_iterators(new Partition());

	PartitionEnumeration partitions = new PartitionEnumeration();

	System.out.println("there are " + partitions.size() + " partitions :");
	for (ipc.Partition p : partitions)
	    test_generic_iterators(p);
	
	System.out.println("there are " + partitions.size() + " partitions :");
        while (partitions.hasMoreElements())
	    test_generic_iterators(partitions.nextElement());
    }

    public static void test_generic_iterators(Partition p) throws InvalidPartitionException
    {
	TypeEnumeration types = new TypeEnumeration(p);
	System.out.println("Partition \"" + p.getName()
		+ "\" contains objects of " + types.size() + " types :");

	for (String t : types) {
	    ObjectEnumeration<ipc.servant> objects = new ObjectEnumeration<ipc.servant>(p, t);
	    System.out.println("\tthere are " + objects.size()
		    + " objects of the \"" + t + "\" type :");

	    for (ipc.Element<ipc.servant> obj : objects) {
		System.out.println("\t\t\"" + obj.name + "\" : "
			+ Core.objectToString(obj.reference, Core.CORBALOC));
	    }
	}
            
	while (types.hasMoreElements()) {
	    String type = types.nextElement();
	    ObjectEnumeration objects = new ObjectEnumeration(p, type);
	    System.out.println("\tthere are " + objects.size()
		    + " objects of the \"" + type + "\" type :");

	    while (objects.hasMoreElements()) {
		ipc.Element obj = objects.nextElement();
		System.out.println("\t\t\"" + obj.name + "\" : "
			+ Core.objectToString(obj.reference, Core.CORBALOC));
	    }
	}
    }

    public static void test_typed_iterators() throws InvalidPartitionException
    {
	test_typed_iterators(new Partition());

	PartitionEnumeration partitions = new PartitionEnumeration();

	for (ipc.Partition p : partitions)
	    test_typed_iterators(p);
    }

    public static <Type extends ipc.servant> 
    void test_typed_iterator(Partition p, Class<Type> type) throws InvalidPartitionException
    {
	System.out.println("Checking for the objects of the '" + type.getName()
		+ "' type in the '" + p.getName() + "' partition ...");
	ObjectEnumeration<Type> objects = new ObjectEnumeration<Type>(p, type);
	System.out.println("\tthere are " + objects.size() + " objects of the '"
		+ type.getName() + "' type :");

	for (ipc.Element<Type> obj : objects) {
	    System.out.println("\t\t\"" + obj.name + "\" : "
		    + Core.objectToString(obj.reference, Core.CORBALOC));
	}
    }

    public static void test_typed_iterators(Partition p) throws InvalidPartitionException
    {
	test_typed_iterator(p, ipc.server.class);
	test_typed_iterator(p, ipc.person.class);
	test_typed_iterator(p, ipc.employee.class);
    }

    public static void test_lookup(String name) throws InvalidPartitionException
    {
	Partition p = new Partition();

	for (int i = 0; i < 5; i++) {
	    try {
		ipc.server tt = p.lookup(ipc.server.class, name);
                tt.ping_s("Test string");
		System.out.println("ping_s was called successfully");
	    }
	    catch (Exception e) {
		System.out.println("System exception : " + e);
	    }
	    try {
		Thread.sleep(2000);
	    }
	    catch (java.lang.InterruptedException e) {
		System.err.println("Interrupted");
	    }
	}
    }

    public static void test_publish(String name)
	    throws InvalidPartitionException
    {
	TestServant t = new TestServant(new Partition(), name);

	System.out.println("isPublished() returns '" + t.isPublished() + "'");

	System.out.println("Publishing '" + name + "' object");
	t.publish();

	System.out.println("isPublished() returns '" + t.isPublished() + "'");
	test_lookup(name);

	System.out.println("Withdrawing '" + name + "' object");
	t.withdraw();

	System.out.println("isPublished() returns '" + t.isPublished() + "'");

	test_lookup(name);

	System.out.println("Publishing '" + name + "' object");
	t.publish();

	test_lookup(name);

	System.out.println("Withdrawing '" + name + "' object");
	t.withdraw();

	System.out.println("Publishing '" + name + "' object");
	t.publish();

	test_lookup(name);

	System.out.println("isPublished() returns '" + t.isPublished() + "'");

	System.out.println("Withdrawing '" + name + "' object");
	t.withdraw();

	System.out.println("isPublished() returns '" + t.isPublished() + "'");
    }

    public static void test_objects() throws InvalidPartitionException
    {
	Partition p = new Partition();
	System.out.println("isValid() returns '" + p.isValid() + "'");
	System.out.println("(new Partition(\"test\")).isValid() returns '" + (new Partition("test")).isValid() + "'");

	Person p1 = new Person(p, "John", (short) 21);
	p1.publish();
	Person p2 = new Person(p, "Jake", (short) 25);
	p2.publish();

	Employee e1 = new Employee(p, "Sara", (short) 27, (short) 5000);
	e1.publish();
	Employee e2 = new Employee(p, "Clara", (short) 29, (short) 4700);
	e2.publish();

	TestServant t1 = new TestServant(new Partition(), "test1");
	t1.publish();
	TestServant t2 = new TestServant(new Partition(), "test2");
	t2.publish();

	test_typed_iterators();

    }
    
    static class TestThread extends Thread {
        private String name;
        private int timeout;
        
        TestThread(String name, int timeout) {
            this.name = name;
            this.timeout = timeout;
        }
        
        public void run(){
            Partition p = new Partition();
            try {
                ipc.server tt = p.lookup(ipc.server.class, name);
                System.out.println("got CORBA object " + tt.hashCode());
                Thread.sleep(timeout);
                System.out.println("invoking ping on CORBA object " + tt.hashCode());
                tt.ping_s("Test string");
                System.out.println("ping_s was called successfully");
            }
            catch (Exception e) {
                System.out.println("System exception : " + e);
            }
        }
    }
    
    public static void test_cache(String name) throws InvalidPartitionException, InterruptedException
    {
        TestThread t1 = new TestThread(name, 5000);
        TestThread t2 = new TestThread(name, 6000);
        
        t1.start();
        t2.start();
        
        t1.join();
        t2.join();
    }

    public static void main(String args[])
    {
	try {
	    test_cache(args[0]);
	    
	    test_generic_iterators();
	    test_lookup(args[0]);

	    test_publish(args[1]);
	    test_objects();
	    
	    System.out.println(
		    ipc.Core.constructReference(
			    "initial", "ipc/partition", "initial", "pcatd88.cern.ch", 12345));
	}
	catch (Exception ex) {
	    System.err.println(ex);
	}
    }
}
