import ipc.*;

class Person<T extends ipc.person> extends ipc.NamedObject<ipc.person>
	implements personOperations
{
    private final short age;

    Person(Partition partition, String name, short age)
    {
	super(partition, name);
	this.age = age;
    }

    public java.lang.String Name()
    {
	return name;
    }

    public short Age()
    {
	return age;
    }
}
