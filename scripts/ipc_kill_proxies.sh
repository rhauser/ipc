#!/bin/sh

function print_usage
{
	echo "start_ipc_proxies: utility to start IPC proxy applications on all hosts used by the partition"
	echo 
	echo "Usage: "
	echo "dvs_init [database_file] partition"
	echo "database_file 		configuration database"
	echo "               		(you must specify it unless you have TDAQ_DB_DATA defined)"
	echo "               		(can be specified relatively to cwd or to TDAQ_DB_PATH)"
	echo "partition 		selected partition (required parameter)"
}

#------------------------------------------------------------
# parse command line
#------------------------------------------------------------

while (test $# -gt 0 ); do
  case "$1" in
    -h | --help)
	print_usage
	exit 0
    ;;

    *.xml)
	TDAQ_DB_DATA=$1
	shift
    ;;

    *)
	partition=$1
	shift
    ;;
  esac
done

if test -z "${TDAQ_DB_DATA}"
  then
	echo "Error: Neither database file parameter nor TDAQ_DB_DATA environment are defined."
	print_usage
	exit 1
fi

if test -z "$partition"
  then
	echo "Error: partition parameter not specified. Please specify a partition you want to run."
	print_usage
	exit 1
fi

TDAQ_PARTITION=$partition
export TDAQ_PARTITION TDAQ_DB_DATA

#------------------------------------------------------------
# get TDAQ environment
#------------------------------------------------------------

if test -r "${TDAQ_INST_PATH}/share/bin/get_tdaq_env.sh"
then
	. ${TDAQ_INST_PATH}/share/bin/get_tdaq_env.sh || { echo "setup_daq: can not set TDAQ envronment variables. Exiting." ; exit 1 ; } ;
else
	echo "Can not access ${TDAQ_INST_PATH}/share/bin/get_tdaq_env.sh. Did you set up TDAQ SW release?"
	exit 1
fi

check_with()
{
    count=0
    until $@
    do
	sleep 1
	count=`expr $count + 1`

	if test $count -eq 15
	then
	    echo "Timeout!"
	    echo "(Server was not started)"
	    exit 1
	fi
    done
}

kill_proxies()
{
    while read LINE ; do
	echo " waiting for ipc proxy to be killed on $LINE ..."

	if test $LINE = $hostname ; then
	     killall ipc_proxy &
	else
	     ssh -nfx $LINE "/bin/sh -c 'killall ipc_proxy' 2>/dev/null"
	fi
    done
}

echo -n "Getting partition default host:  "
export TDAQ_ERS_DEBUG_LEVEL=0
defhostname=`dvs_print_def_host -d ${TDAQ_DB_DATA} -p $partition` || { echo "Failed to get local host. Check your database" ; exit 1 ; }
hostname=`echo $defhostname | sed 's/^.* //'`
logincmd=`echo $defhostname | sed 's/ .*$//'`
if test "$defhostname" = ""; then hostname=`hostname -f`; fi
echo $hostname | kill_proxies
echo "Getting all hosts used in the partition:"
dvs_get_hosts -d ${TDAQ_DB_DATA} -p $partition | kill_proxies

echo "done."
