package ipc;

import java.util.*;

abstract class Cache
{
    public interface Resolver {
    	org.omg.CORBA.Object resolve() throws Exception;
        String uniqueName();
    }
    
    static class Item
    {
	private Resolver resolver;
	private org.omg.CORBA.Object object;

	Item(Resolver resolver, org.omg.CORBA.Object object) {
	    this.resolver = resolver;
	    this.object = object;
	}
        
	org.omg.CORBA.Object recover() throws Exception {
            return (this.object = this.resolver.resolve());
        }
    }
    
    static class NotInCacheException extends java.lang.Exception
    {
	/**
	 * Constructs a NotInCacheException with no detail message.
	 */
	public NotInCacheException()
	{
	    super("Cache does not contain the given object");
	}
    }

    private static Map<String, Item> references = new Hashtable<>();

    static synchronized org.omg.CORBA.Object get(Resolver resolver) throws Exception
    {
	Item it;
	it = references.get(resolver.uniqueName());

	if (it != null) {
	    ers.Logger.debug(1, "'" + resolver.uniqueName() + "' object was found in cache");
	    return it.object;
	}
	
	ers.Logger.debug(1, "'" + resolver.uniqueName() + "' object is not in cache");
	
        org.omg.CORBA.Object object = resolver.resolve();
        
	ers.Logger.debug(1, "'" + resolver.uniqueName() + "' object is added to cache");
        references.put(resolver.uniqueName(), new Item(resolver, object));
        
	return object;
    }
    
    static synchronized org.omg.CORBA.Object recover(org.omg.CORBA.Object object) throws Exception
    {
	for (Iterator<Item> it = references.values().iterator(); it.hasNext();) {
	    Item item = it.next();

	    if (Core.equals(item.object, object)) {
		return item.recover();
	    }
	}
	
	throw new NotInCacheException();
    }
 }
