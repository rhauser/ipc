package ipc;

/**
 * 
 * @author Serguei Kolos
 * This is a helper class used to represent remote objects references in the
 * <tt>ObjectEnumeration</tt>
 *
 * @param <Type> type of IPC object. A valid type is a Java 
 * 		<tt>abstract interface</tt> which is generated from the
 * 		respective IDL <tt>interface</tt> declaration.
 * 
 * @see ObjectEnumeration
 */
public class Element<Type extends org.omg.CORBA.Object>
{
    Element(String name, Type reference)
    {
        this.name = name;
        this.reference = reference;
    }

    /**
     * This is the CORBA reference to remote IPC object.
     */
    public final Type	reference;
    
    /**
     * Contains the ID which is associated with the respective 
     * IPC object in the IPC Naming Service.
     */
    public final String name;    
}
