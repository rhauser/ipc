#include <iostream>

#include <sys/resource.h>
#include <sys/socket.h>
#include <unistd.h>

int main()
{
	
#ifdef  RLIMIT_NOFILE                   // if there is a way to know
        struct rlimit rlp;              // how many sockets can be opend
        getrlimit(RLIMIT_NOFILE,&rlp);  // simultaneously - get this number
	
        int current = rlp.rlim_cur;
        
	if ( rlp.rlim_cur < rlp.rlim_max )
	{
		rlp.rlim_cur = rlp.rlim_max;
        	setrlimit(RLIMIT_NOFILE,&rlp);
        	getrlimit(RLIMIT_NOFILE,&rlp);
	}
        std::cout << "Default # of open files per one process is " << current << std::endl;
        std::cout << "The maximum # is " << rlp.rlim_max << std::endl;
#endif 

	int fds[65535];
	int i = 0;
	while( ( fds[i] = socket(AF_INET, SOCK_STREAM, 0) ) > 0 )
		i++;
        
        std::cout << "The limit can be effectively set to " << fds[--i] + 1 << std::endl;
        
        while ( i >= 0 ){
		close( fds[i--] );
	}
	return 0;
}

