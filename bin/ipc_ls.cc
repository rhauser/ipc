////////////////////////////////////////////////////////////////////////
//    ipc_ls.cc
//
//    IPC listing main function
//
//    Sergei Kolos,    January 2000
//
//    description:
//      Lists services registered in the IPC naming service
//
////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <algorithm>

#include <chrono>

#include <ers/ers.h>

#include <cmdl/cmdargs.h>

#include <ipc/alarm.h>
#include <ipc/core.h>
#include <ipc/applicationcontext.h>
#include <ipc/partition.h>
#include <ipc/threadpool.h>

using std::chrono::system_clock;

namespace 
{    
    struct Tracer
    {
    	Tracer( bool trace )
          : m_trace( trace ),
            m_alarm( 1, boost::bind( &Tracer::checkup, this ) )
        { ; }
        
        void begin( const std::string & name )
        {
            boost::mutex::scoped_lock lock( m_mutex );
            m_operations.insert( std::make_pair( name, boost::posix_time::microsec_clock::local_time() ) );
        }
        
    	void end( const std::string & name, bool valid )
        {
            boost::mutex::scoped_lock lock( m_mutex );
	    if ( m_trace ) {
		if ( valid )
		    std::clog << "Reply from the '" << name << "' object received." << std::endl;
		else
		    std::clog << "Failed to get response from the '" << name << "' object." << std::endl;
	    }
            m_operations.erase( m_operations.find( name ) );
        }
        
      private:
      	bool checkup()
        {
            static const boost::posix_time::time_duration 
            		timeout( boost::posix_time::seconds( 1 ) );
                        
	    if ( !m_trace )
            	return false;
                
            boost::mutex::scoped_lock lock( m_mutex );
            
            Operations::const_iterator it = m_operations.begin();
            
            boost::posix_time::ptime now( boost::posix_time::microsec_clock::local_time() );
            for ( ; it != m_operations.end(); ++it ) 
            {
                if ( ( now - it->second ) > timeout ) {
		    std::clog << "Still waiting for response from the '" 
				<< it->first << "' object ... " << std::endl;
		}
            }
            
            return true;
        }
        
      private:
	typedef std::map<std::string,boost::posix_time::ptime> Operations;
        
        boost::mutex	m_mutex;
        Operations	m_operations;
        bool		m_trace;
        IPCAlarm	m_alarm;
    };
    
    struct Item : public IPCApplicationContext
    {
	Item( 	const std::string & name,
        	const std::string & type = "",
                ipc::servant_ptr servant = ipc::servant::_nil() )
	  : IPCApplicationContext( servant ),
	    m_object_name( name ),
	    m_object_type( type ),
	    m_is_context( type.empty() && CORBA::is_nil( servant ) )
	{ ; }

	std::string	m_object_name;
	std::string	m_object_type;
	bool		m_is_context;
    };

    class PartitionContent
    {
      public:
	PartitionContent (  const IPCPartition & partition,
			    const std::string & interface_name,
			    bool interface_name_given,
			    bool list_partitions_only,
			    bool list_interfaces_only,
			    bool trace );

	void print( bool long_format, bool show_hidden );

      private:
	void processObject( const std::string & name,
			    const std::string & type,
			    ipc::servant_ptr object );

	void addItem( const Item & );

      private:
        // map is used to sort the items
	typedef std::map<std::string,Item> Items;

      private:
	boost::mutex	m_mutex;
	Items		m_items;
	unsigned int	m_max_name_length;
	unsigned int	m_max_host_length;
	unsigned int	m_max_owner_length;
	bool		m_trace;
        Tracer		m_tracer;
    };

    PartitionContent::PartitionContent( const IPCPartition & partition,
					const std::string & interface_name,
					bool interface_name_given,
					bool list_partitions_only,
					bool list_interfaces_only,
					bool trace )
      : m_max_name_length (0),
	m_max_host_length (0),
	m_max_owner_length(0),
	m_trace( trace ),
        m_tracer( trace )
    {
	//////////////////////////////////////////////
	// Get information for the target partition
	//////////////////////////////////////////////

	if ( m_trace )
	    std::clog << "Connecting to the \"" << partition.name() 
            	<< "\" partition server ..." << std::flush;

	ipc::partition_var pi = partition.getImplementation();

	if ( m_trace )
	    std::clog << "done." << std::endl;

	// "\1" as type name assures that this item will always be printed in the first place
        addItem( Item( partition.name(), "\1", pi ) );
	//////////////////////////////////////////////
	// Get information for user partitions
	// if current partition is the initial one
	//////////////////////////////////////////////

	if ( list_partitions_only )
	{
	    //////////////////////////////////////////////
	    // Get list of partitions
	    //////////////////////////////////////////////
	    std::list< IPCPartition > pl;
	    if ( m_trace )
		std::clog << "Getting list of partitions ..." << std::flush;

	    IPCPartition::getPartitions( pl );
	    if ( m_trace )
		std::clog << "done." << std::endl;

	    for ( std::list< IPCPartition >::iterator pit = pl.begin(); pit != pl.end(); pit++ )
	    {
		try {
		    addItem( Item( (*pit).name(), "partition", pit->getImplementation() ) );
		}
		catch( daq::ipc::Exception & ex ) {
		    if ( m_trace )
			std::clog << "partition '" << pit->name()
			    << "' does not exist" << std::flush;
		}
	    }

	    return;
	}

	//////////////////////////////////////////////
	// Get information for the published objects
	//////////////////////////////////////////////

	std::list<std::string> c_names;

	if ( m_trace )
	    std::clog << "Getting list of object types ..." << std::flush;

	partition.getTypes( c_names );

	if ( m_trace )
	    std::clog << "done." << std::endl;

	IPCThreadPool threadpool( 100 );
	for ( std::list< std::string >::iterator cit = c_names.begin(); cit != c_names.end(); cit++ )
	{
	    if ( interface_name_given )
	    {
		if ( cit -> find( interface_name ) != 0 )
		{
		    continue ;
		}
	    }

	    addItem( Item( *cit ) );

	    if ( list_interfaces_only && !interface_name_given )
	    {
		continue ;
	    }

	    std::map<std::string,ipc::servant_var> objects;

	    if ( m_trace )
		std::clog << "Getting list of objects of the '" << *cit << "' type ..." << std::flush;

	    partition.getObjects<ipc::servant,ipc::use_cache,ipc::unchecked_narrow>( objects, *cit );

	    if ( m_trace )
		std::clog << "done." << std::endl;

	    std::map<std::string,ipc::servant_var>::iterator it = objects.begin();

	    while( it != objects.end() )
	    {
		threadpool.addJob( boost::bind( &PartitionContent::processObject, this, it->first, *cit, it->second ) );
		it++;
	    }
	    threadpool.waitForCompletion();
	}
    }

    void PartitionContent::processObject( const std::string & name,
					  const std::string & type,
					  ipc::servant_ptr object )
    {
	m_tracer.begin( name );
        Item item( name, type, object );
	addItem( item );
	m_tracer.end( name, item.m_valid );
    }
    
    void PartitionContent::addItem( const Item & item )
    {
	boost::mutex::scoped_lock lock( m_mutex );
        std::string key( item.m_object_type + item.m_object_name );
	m_items.insert( std::make_pair( key, item ) );

	if ( !item.m_is_context )
	{
	    m_max_name_length  = std::max( m_max_name_length,	(unsigned int)item.m_object_name.size() );
	    m_max_host_length  = std::max( m_max_host_length,	(unsigned int)item.m_host.size() );
	    m_max_owner_length = std::max( m_max_owner_length,	(unsigned int)item.m_owner.size() );
	}
    }

    void PartitionContent::print( bool long_format, bool show_hidden )
    {
	Items::iterator it;

	int prefix_w = 0;
	int separator_w = 8;
	char tbuff[128];
	for( it = m_items.begin(); it != m_items.end(); ++it )
	{
	    if ( it->second.m_is_context )
	    {
		printf("   %s:\n", it->second.m_object_name.c_str() );
		continue;
	    }

	    if ( it->second.m_valid || show_hidden )
	    {
		if ( long_format && it->second.m_valid )
		{
		    std::time_t t = system_clock::to_time_t(it->second.m_time);
		    std::tm tm = *localtime_r(&t, &tm);

		    std::strftime(tbuff, 128, "%Y-%b-%d %H:%M:%S", &tm);
                    
                    printf("%*s%-*s %*s %-*s %8u %-*s %s %d %d\n",
				prefix_w, it->second.m_valid ? "" : "*",
				m_max_name_length, it->second.m_object_name.c_str(),
				separator_w, "",
				m_max_host_length, it->second.m_host.c_str(),
				it->second.m_pid,
				m_max_owner_length, it->second.m_owner.c_str(),
				tbuff,
				it->second.m_debug_level,
				it->second.m_verbosity_level );
		}
		else
		{
		    printf("%*s%s\n", prefix_w, it->second.m_valid ? "" : "*", it->second.m_object_name.c_str() );
		}
	    }
	    prefix_w = 8;
	    separator_w = 0;
	}
    }
}

int main(int argc, char ** argv)
{    
    try {
	std::list< std::pair< std::string, std::string > > opt = IPCCore::extractOptions( argc, argv );
	opt.push_front( std::make_pair( std::string("scanGranularity"), std::string("1") ) );
	opt.push_front( std::make_pair( std::string("inConScanPeriod"), std::string("2") ) );
        IPCCore::init( opt );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }
    
    CmdArgStr	partition_name	('p', "partition", "partition-name", "List applications for this partition.");
    CmdArgStr	interface_name	('i', "interface", "interface-name", "Show only applications implementing given interface.");
    CmdArgBool	long_format	('l', "long", "Use a long listing format.");
    CmdArgBool	show_hidden	('a', "all", "Show dangling objects that are normally hidden.");
    CmdArgBool	trace		('t', "trace", "Trace execution.");
    CmdArgBool	show_reference	('R', "reference", "Print the IPC initial reference.");
    CmdArgBool	partitions_only ('P', "partitions", "Print only list of IPC partitions (ignored if -p is given).");
    CmdArgBool	interfaces_only ('I', "interfaces", "Print only list of know interfaces (ignored if -i is given).");

    // Declare command object and its argument-iterator
    CmdLine    cmd(*argv, &partition_name, &show_hidden, 
    			  &long_format, &show_reference, &trace, &interface_name,
                          &partitions_only, &interfaces_only, NULL);
    CmdArgvIter    arg_iter(--argc, ++argv);
    
    cmd.description(	"List applications for the given partition."
                        "Sort entries alphabetically."
                        "In the long format mode shows: NAME HOST PID USER TIME DEBUG_LEVEL VERBOSITY_LEVEL" );
    
    interface_name = "";
    cmd.parse(arg_iter);

    if ( show_reference.flags() && CmdArg::GIVEN )
    {
	std::cout << "Initial reference is " << ipc::util::getInitialReference() << std::endl;
    }
    
    IPCPartition partition( (const char *)partition_name );    
    
    try {
        PartitionContent pc( partition,  (const char*)interface_name,
        				 ( interface_name.flags()  && CmdArg::GIVEN ),
        				 ( partitions_only.flags() && CmdArg::GIVEN ),
        				 ( interfaces_only.flags() && CmdArg::GIVEN ),
                                         ( trace.flags()           && CmdArg::GIVEN ) );
	pc.print( long_format.flags() && CmdArg::GIVEN, show_hidden.flags() && CmdArg::GIVEN );
    }
    catch( daq::ipc::InvalidPartition & ex ) {
	std::cerr << "ERROR [ipc_ls] Can't get information for the '" << partition.name() << "' partition\n" 
        	     "Reason: " << ex << std::endl;
        return 1;
    }

    return 0;
}
