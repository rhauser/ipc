////////////////////////////////////////////////////////////////////////
//    ipc_cln.cc
//
//    IPC clean up main function
//
//    Sergei Kolos,    January 2000
//
//    description:
//      Clean up dangling references from the IPC naming service
//
////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <regex>
#include <string>

#include <ers/ers.h>

#include <cmdl/cmdargs.h>

#include <ipc/partition.h>
#include <ipc/util.h>
#include <ipc/core.h>

CmdArgBool trace ('t', "trace", "trace execution.");

template <class T, template < class > class VP>
struct remove_dangling
{
    template <ipc::Fun3 fun>
    static
    typename T::_ptr_type resolve( const std::string & n1, 
    				   const std::string & n2, 
                                   const std::string & n3 )
    {
	if ( trace )
	    std::cout << ">> resolving reference to the \"" 
            	<< n2 << "\" object ... " << std::flush;
	
        CORBA::Object_var obj = fun( n1, n2, n3 );
	if ( trace )
	    std::cout << "done" << std::endl;
	
	if ( trace )
	    std::cout << "done" << std::endl
            	<< ">> cheking existence of the \"" 
                << n2 << "\" object ... " << std::flush;
	
        typename T::_var_type tt = VP<T>::validate( obj );
	
        if ( CORBA::is_nil( tt ) )
	{
	    if ( trace )
		std::cout << "error" << std::endl;
	    
            if ( trace )
		std::cout << ">> removing dangling reference to the \"" 
                	  << n2 << "\" object ... " << std::flush;
	    try {
		ipc::util::unbind( n1, n2, n3 );
            }
            catch( daq::ipc::Exception & ex ) {
            	ers::error( ex );
            }
	    if ( trace )
		std::cout << "done" << std::endl;
	}
	else {
	    if ( trace )
		std::cout << "ok" << std::endl;

	    return T::_duplicate(tt);
	}

	return T::_nil();
    }
    
    template <ipc::Fun1 fun>
    static
    typename T::_ptr_type resolve( const std::string & n )
    {
	typename T::_var_type ref;
        
	if ( trace )
	    std::cout << ">> resolving reference to the \"" 
            	<< n << "\" partition ... " << std::flush;
        try {
	    CORBA::Object_var obj = fun( n );
	    if ( trace )
		std::cout << "done" << std::endl 
                	<< ">> cheking existence of the \"" 
                        << n << "\" partition ... " << std::flush;
	    ref = VP<T>::validate( obj );
	}
        catch( daq::ipc::Exception & ex ) {
	    if ( trace )
		std::cout << "error" << std::endl;
	    ers::error( ex );
        }
        	
	if ( CORBA::is_nil( ref ) )
	{
	    if ( trace )
		std::cout << "error" << std::endl 
                	<< ">> removing dangling reference to the \"" 
                        << n << "\" partition ... " << std::flush;
	    try {
            	ipc::util::unbind( IPCPartition().name(), n, std::string() );
            }
            catch( daq::ipc::Exception & ex ) {
            	ers::error( ex );
            }
	    if ( trace )
		std::cout << "done" << std::endl;
	}
	else {
	    if ( trace )
		std::cout << "ok" << std::endl;

	    return T::_duplicate(ref);
	}
            
	return T::_nil();
    }
};
    
void clean( IPCPartition & p )
{
    if ( trace )
  	std::cout << ">> cleaning partition \""
        	<< p.name() << "\" ... " << std::endl;

    std::list<std::string> t_names;
    
    try {
    	p.getTypes( t_names );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::error( ex );
    }
    
    for ( std::list<std::string>::iterator it = t_names.begin(); it != t_names.end(); it++ )
    {    
	if ( trace )
	    std::cout << ">> cheking objects of the \"" 
            	<< *it << "\" type ..." << std::endl;

	try {
	    std::map<std::string,ipc::servant_var> objects;
	    p.getObjects<ipc::servant,remove_dangling,ipc::non_existent>( objects, *it );
	}
	catch( daq::ipc::Exception & ex ) {
	    ers::error( ex );
	}
                
	if ( trace )
	    std::cout << ">> done." << std::endl;
    }
    
    if ( trace )
        std::cout << ">> done." << std::endl;
}

int main( int argc, char** argv )
{
    try {
	std::list< std::pair< std::string, std::string > > opt = IPCCore::extractOptions( argc, argv );
	opt.push_front( std::make_pair( std::string("scanGranularity"), std::string("1") ) );
	opt.push_front( std::make_pair( std::string("inConScanPeriod"), std::string("2") ) );
        IPCCore::init( opt );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }
     
    CmdArgBool	all ('a', "all", "clean all partitions.");
    CmdArgStr	partition_name ('p', "partition", "partition-name", "partition to clean up.");

    CmdLine	cmd(*argv, &trace, &partition_name, &all, NULL);
    CmdArgvIter	arg_iter(--argc, ++argv);

    cmd.description( "Removes dangling objects references from the given partition");
  
    cmd.parse( arg_iter );

    try {
        CORBA::Object_ptr ref = ipc::util::resolveInitialReference("");
        if (CORBA::is_nil(ref)) {
            std::cerr << "FATAL: IPC initial partition is not available" << std::endl;
            return 1;
        }
        std::string strref = IPCCore::objectToString(ref, IPCCore::Corbaloc );
        std::string proxyref = std::regex_replace(strref, std::regex(
                "%ffipc/partition%00initial/ipc/partition/initial"),
                "%ffIPCGateway/Proxy%00private");
        if (proxyref != strref) {
            CORBA::Object_var proxy = IPCCore::stringToObject( proxyref );
            if ( not CORBA::is_nil(proxy) && not proxy -> _non_existent() ) {
                std::cerr << "FATAL: ipc_cln cannot be used on a host connected"
                        " to a private network behind the IPC gateway proxy." << std::endl;
                return 1;
            } else {
                ERS_DEBUG(1, "Initial IPC object does not belong to a proxy application");
            }
        }
    }
    catch( daq::ipc::Exception & ex ) {
        ERS_DEBUG(1, "Partition object does not belong to an IPC gateway proxy, " << ex);
    }
    catch ( CORBA::SystemException & ) {
        ERS_DEBUG(1, "Partition object does not belong to an IPC gateway proxy");
    }

    if ( partition_name.flags() & CmdArg::GIVEN )
    {
  	IPCPartition  p( partition_name );
	clean( p );
    }
    else
    {
	std::list < IPCPartition> pl;
	IPCPartition::getPartitions<remove_dangling>( pl );
        
  	if ( all.flags() & CmdArg::GIVEN )
	{	 
	    //
	    // Cleaning dangling objects in all partitions
	    //
  	    for ( std::list<IPCPartition>::iterator it = pl.begin(); it != pl.end(); it++ )
	    {
		clean( *it );			
	    }
	}

  	//
	// Cleaning default partition
	//
	
	IPCPartition  p;
	clean( p );			
    }  	

    return 0;
}
