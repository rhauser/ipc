#include <iostream>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <ers/ers.h>

#include <cmdl/cmdargs.h>

#include <ipc/partition.h>
#include <ipc/core.h>

using namespace std;

int main(int argc, char ** argv)
{    
    CmdArgStrList	hosts('H', "hosts", "[host-name ...]", "list of hosts for the initial IPC server.", 
    				CmdArg::isREQ | CmdArg::isLIST);
    CmdArgIntList	ports('P', "ports", "[port-number ...]", "list of port numbers for the initial IPC server.",
    				CmdArg::isREQ | CmdArg::isLIST);
    CmdArgBool		verify('V', "verify", "Test existence of the initial IPC server.");
    CmdArgBool		quiet('q', "quiet", "Print nothing except the reference.");

    // Initialise IPC
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }
    
    // Declare command object and its argument-iterator
    CmdLine	cmd(*argv, &hosts, &ports, &verify, &quiet, NULL);
    CmdArgvIter	arg_iter(--argc, ++argv);
    
    cmd.description( "Constructs IPC initial reference from host name and port number." );
    
    cmd.parse(arg_iter);
    
    if ( hosts.count() != ports.count() )
    {
	cerr << "ERROR [ipc_mk_ref] The numbers of hosts and ports parameters must be the same" << endl;
	return 1;
    }
    
    if ( hosts.count() == 0 )
    {
	cerr << "ERROR [ipc_mk_ref] At least one host and one port parameter must be provided" << endl;
	return 2;
    }
    
    std::vector<std::pair<std::string,int> > endpoints;
    for ( unsigned int i = 0; i < hosts.count(); i++ )
    {
	endpoints.push_back( std::make_pair( (const char *)hosts[i], (int)ports[i]) );
    }
        
    string reference;
    try {
        reference = ipc::util::constructReference(	ipc::partition::default_name,
        						ipc::partition::default_name,
    							ipc::util::getTypeName<ipc::partition>(), 
							endpoints );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 2;
    }

    std::cout << reference << std::endl;
    
    if ( verify.flags() && CmdArg::GIVEN )
    {
        try {
	    CORBA::Object_var object = IPCCore::stringToObject( reference );
	    ipc::partition_var partition = ipc::partition::_narrow( object );

	    if ( CORBA::is_nil( partition ) )
	    {
		if ( !quiet )
		    cerr << "The initial IPC server does not exist for the \"" 
                    	 << reference << "\" reference" << endl;
		return 3;
	    }
            
	    ipc::servant::ApplicationContext_var appc;
	    appc = partition -> app_context();
	    if ( !quiet )
		cout	<< "The initial IPC server has been started by " << appc->owner
			<< " at " << boost::posix_time::from_time_t( appc -> time ) << endl;
	}
	catch( daq::ipc::Exception & ex ) {
	    ers::fatal( ex );
	    return 3;
	}
	catch ( CORBA::SystemException & )
	{
	    if ( !quiet )
		cerr << "The initial IPC server does not exist for the \"" 
                	<< reference << "\" reference" << endl;
	    return 3;
	}
    }
        
    return 0;
}
