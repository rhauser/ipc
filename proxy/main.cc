////////////////////////////////////////////////////////////////////////////
//      main.cc
//
//      Implementation of the IPC proxy application
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains main function of the IPC proxy
//
////////////////////////////////////////////////////////////////////////////
#include <sys/stat.h>
#include <unistd.h>

#include <fstream>

#include <ipc/core.h>
#include <ipc/alarm.h>
#include <ipc/exceptions.h>
#include <ipc/pluginfactory.h>
#include <ipc/signal.h>
#include <ipc/proxy/manager.h>
#include <ipc/proxy/proxy.h>

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/exception.hpp>

#include <cmdl/cmdargs.h>

ERS_DECLARE_ISSUE(      ipc,
                        ProxyAlreadyExist,
                        "IPC proxy is already running on this host",
			ERS_EMPTY
                  )

namespace bfs = boost::filesystem;

namespace 
{
    void
    test_create_file( const std::string & file_name ) 
    {
    	bfs::path path( file_name );
        
        if ( bfs::exists( path ) )
        {
            ERS_DEBUG( 1, "the '" << path.string() << "' already exists" );
	    if ( bfs::is_directory( path ) )
	    {
		throw daq::ipc::CannotOpenFile( ERS_HERE, path.string(), "this is directory name" );
	    }
            return;
        }        
        
        bfs::path directory( path.parent_path() );
        try {            
	    bfs::create_directories( directory );
            std::string tmp = directory.string();
            ::chmod( tmp.c_str(), 0775 );
        }
        catch( bfs::filesystem_error & ex )
        {
            throw daq::ipc::CannotCreateDirectory( ERS_HERE, directory.string(), ex.what() );
        }
        
        std::ofstream file( file_name.c_str() );
	if ( !file )
	{
	    throw daq::ipc::CannotOpenFile( ERS_HERE, path.string(), strerror(errno) );
	}
    }

    bool
    cleanup_action( void * )
    {
	IPCSingleton<IPCProxyManager>::instance().cleanup( );
	return true;
    }

    void
    init( const std::string & args )
    {
        std::string plugins = args + ':';
        std::string::size_type start = 0, end;
        while( ( end = plugins.find( ':', start ) ) != std::string::npos )
        {
            std::string plugin = plugins.substr( start, end - start );
            start = end + 1;

            if ( plugin.empty() )
                continue;

            try {
                ERS_DEBUG( 1, "loading the '" << plugin << "' plugin" );
                IPCSingleton< IPCPluginFactory<IPCProxyFactoryBase> >::instance().load( plugin );
                ERS_DEBUG( 1, "the '" << plugin << "' plugin was successfully loaded" );
            }
            catch( daq::ipc::PluginException & ex ) {
                ers::error( ex );
            }
            catch( daq::ipc::IgnorePluginException & ex ) {
                ers::debug( ex, 1 );
            }
            catch(...) {
                ers::error( daq::ipc::PluginException( ERS_HERE, plugin, "unknown" ) );
            }
        }
    }

}

int main(int argc, char** argv)
{
    // If environment is not set explicitly we don't want
    // to use the default values of it
    if ( !getenv( "TDAQ_IPC_CLIENT_INTERCEPTORS" ) )
    {
    	::setenv( "TDAQ_IPC_CLIENT_INTERCEPTORS", "", 1 );
    }
    if ( !getenv( "TDAQ_IPC_SERVER_INTERCEPTORS" ) )
    {
	::setenv( "TDAQ_IPC_SERVER_INTERCEPTORS", "", 1 );
    }

    {
    	// Do not allow to use the configuration file since it may contain
        // 'endpoint' options which must not be used for the proxy
	::setenv( "OMNIORB_CONFIG", "this_file_must_not_exist", 1 );
    }
    
    std::list< std::pair< std::string, std::string > > opt;
    try {
        IPCCore::init( opt );
	if ( IPCProxy::exists() )
	{
	    ers::fatal( ipc::ProxyAlreadyExist( ERS_HERE ) );
	    return 3;
	}
        IPCCore::shutdown( );
    }
    catch( daq::ipc::Exception & ex ) {
        ers::fatal( ex );
        return 3;
    }

    try {
        opt = IPCCore::extractOptions( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
        ers::fatal( ex );
        return 2;
    }
    
    CmdArgStr	plugins ( 'p', "plugins", "plugins-names", "colon separated list of plugins to be used\n"
    								"default value is \"ipcproxy:isproxy\"" );
    CmdArgBool	print_ior ( 'i', "ior", "print self reference." );
    CmdArgInt	cleanup_period( 'c', "cleanup", "cleanup-period", "period (in seconds) for cleaning up the proxy object cache\n"
    								  "default is 3600 seconds, <= 0 will disable the cleanup" );

    // Declare command object and its argument-iterator
    CmdLine cmd( *argv, &plugins, &print_ior, &cleanup_period, NULL );
    CmdArgvIter arg_iter(--argc, ++argv);

    cmd.description( "This program implements the IPC proxy functionality." );

    // default values
    plugins = "ipcproxy:isproxy";
    cleanup_period = 3600;
    
    // Parse arguments
    cmd.parse(arg_iter);

    std::string endpoint = std::string( "giop:unix:" ) + IPC_PROXY_CONNECT_FILE_NAME;
        
    try {
	test_create_file( IPC_PROXY_CONNECT_FILE_NAME );
        opt.push_front( std::make_pair( std::string("endPoint"), endpoint ) );
        opt.push_front( std::make_pair( std::string("maxGIOPConnectionPerServer"), std::string("1") ) );
        opt.push_front( std::make_pair( std::string("clientCallTimeOutPeriod"), std::string("100000") ) );
        opt.push_front( std::make_pair( std::string("oneCallPerConnection"), std::string("0") ) );
        IPCCore::init( opt );
    }
    catch( daq::ipc::Exception & ex ) {
        ers::fatal( ex );
        return 1;
    }
    catch( std::exception & ex ) {
        ers::fatal( daq::ipc::CannotOpenFile( ERS_HERE, IPC_PROXY_CONNECT_FILE_NAME, ex.what() ) );
        return 4;
    }
    
    IPCProxy * proxy = new IPCProxy();
    CORBA::Object_var reference = proxy -> _this();

    if ( print_ior.flags() && CmdArg::GIVEN )
    {
	try {
	    std::string IOR = IPCCore::objectToString( reference, IPCCore::Ior );
	    std::cout << IOR << std::endl;
	}
	catch( daq::ipc::Exception & ex ) {
	    ers::error( ex );
	}
    }
        
    init( (const char *)plugins );
    
    std::unique_ptr<IPCAlarm> action_;
    
    if ( cleanup_period > 0 )
    {
    	action_.reset( new IPCAlarm( cleanup_period, cleanup_action, 0 ) );
    }

    ERS_LOG( "IPC proxy server has been started" );
    
    daq::ipc::signal::wait_for();
    
    proxy->_destroy();
    
    return 0;
}

