////////////////////////////////////////////////////////////////////////////
//      named_main.cc
//
//      Implements generic IPC service proxy application
//
//      Serguei Kolos November 2023
//
////////////////////////////////////////////////////////////////////////////
#include <chrono>
#include <thread>

#include <boost/program_options.hpp>

#include <ers/ers.h>

#include <ipc/core.h>
#include <ipc/partition.h>
#include <ipc/pluginfactory.h>
#include <ipc/signal.h>
#include <ipc/proxy/manager.h>

ERS_DECLARE_ISSUE(ipc,
        ProxyAlreadyExists,
        "The '" << name << "' proxy of '" << type << "' type already exists in '"
                    << partition_name << "' partition",
        ((std::string) name) ((std::string) type) ((std::string) partition_name)
);

ERS_DECLARE_ISSUE(ipc,
        ServerNotFound,
        "No '" << name << "' server of '" << type << "' type is present in '"
                    << partition_name << "' partition",
        ((std::string) name) ((std::string) type) ((std::string) partition_name)
);

ERS_DECLARE_ISSUE(ipc,
        FactoryFailure,
        "Failed to create proxy object of '" << type << "' type",
        ((std::string) type)
);

int main(int ac, char **av) {

    try {
        IPCCore::init(ac, av);
    } catch (daq::ipc::Exception &ex) {
        ers::fatal(ex);
        return 1;
    }

    std::string server_partition_name;
    std::string partition_name;
    std::string plugin_name;
    std::string proxy_name;
    std::string server_name;
    std::string type_name;

    const std::string c_proxy_partition_default_value = "<partition-name>";

    using namespace boost::program_options;
    options_description description("Options");
    description.add_options()
        ("help,h", "produce help message")
        ("wait,w", "wait for the target server to be started")
        ("server-partition-name,r", value<std::string>(&server_partition_name)
                ->default_value(c_proxy_partition_default_value),
            "partition name of the target server")
        ("partition-name,p", value<std::string>(&partition_name)
                ->default_value("initial"),
            "partition name of the proxy")
        ("plugin-name,l", value<std::string>(&plugin_name)->required(),
            "short name of the proxy implementation library, e.g 'isproxy'")
        ("proxy-name,n", value<std::string>(&proxy_name)->required(),
            "proxy name")
        ("server-name,s", value<std::string>(&server_name)->required(),
            "server name")
        ("type-name,t", value<std::string>(&type_name)->default_value(""),
            "server type name, e.g. 'is/repository'. Must be empty for IPC server proxy.");

    bool wait_for_server = false;
    try {
        variables_map arguments;
        store(parse_command_line(ac, av, description), arguments);

        if (arguments.count("help")) {
            std::cout << "IPC named service proxy." << std::endl;
            std::cout << description;
            return 0;
        }

        notify(arguments);

        wait_for_server =  arguments.count("wait");

        if (server_partition_name == c_proxy_partition_default_value) {
            server_partition_name = partition_name;
        }
    }
    catch(std::exception& e) {
        std::cerr << "Error: " << e.what() << "\n";
        std::cout << description;
        return 1;
    }

    try {
        IPCPartition server_partition(server_partition_name);
        IPCPartition proxy_partition(partition_name);

        if (proxy_partition.isObjectValid<
                ipc::servant, ipc::use_cache, ipc::non_existent>(
                proxy_name, type_name)) {
            ers::fatal(ipc::ProxyAlreadyExists(ERS_HERE,
                    proxy_name, type_name, server_partition.name()));
            return 1;
        }

        while (true) {
            try {
                if (server_partition.isObjectValid<
                        ipc::servant, ipc::use_cache, ipc::non_existent>(
                        server_name, type_name)) {
                    break;
                }
            }
            catch (...) {
            }

            if (not wait_for_server) {
                ers::fatal(ipc::ServerNotFound(ERS_HERE,
                        server_name, type_name, server_partition.name()));
                return 1;
            }

            using namespace std::literals;
            std::this_thread::sleep_for(1s);
        }

        ipc::servant_var s = server_partition.lookup<
                ipc::servant, ipc::use_cache, ipc::non_existent>(
                server_name, type_name);

        std::string interface_id = s->_mostDerivedRepoId();

        IPCSingleton<IPCPluginFactory<IPCProxyFactoryBase>>::instance().load(plugin_name);

        CORBA::Object_var proxy = IPCSingleton<IPCProxyManager>::instance().createProxy(
                proxy_partition, proxy_name, interface_id, s);

        if (CORBA::is_nil(proxy)) {
            throw ipc::FactoryFailure(ERS_HERE, interface_id);
        }

        ipc::util::bind(proxy_partition.name(), proxy_name, type_name, proxy);

        ERS_LOG("Proxy server '" << proxy_name << "' of '" << interface_id
                << "' type has been started in '" << proxy_partition.name()
                << "' partition for '" << server_name
                << "' server belonging to '" << server_partition.name() << "' partition");

        daq::ipc::signal::wait_for();

        ipc::util::unbind(proxy_partition.name(), proxy_name, type_name);
    } catch (ers::Issue& e) {
        ers::fatal(e);
        return 1;
    }

    return 0;
}
