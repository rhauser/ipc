////////////////////////////////////////////////////////////////////////
//    ipc_server.cc
//
//    IPC server main function
//
//    Sergei Kolos,    January 2000
//
//    description:
//      Implements naming service for the IPC
//
////////////////////////////////////////////////////////////////////////

#include <errno.h>

#include <fstream>
#include <string>

#include <cmdl/cmdargs.h>

#include <ipc/core.h>
#include <ipc/signal.h>
#include <ipc/server/Partition_impl.h>

#include <pmg/pmg_initSync.h>


namespace daq {
    /*! \class daq::ipc::PartitionAlreadyExist
     *	\brief This issue is reported if an IPC server application is started for the partition which already exist.
     */
    ERS_DECLARE_ISSUE(	    ipc,
			    PartitionAlreadyExist,
			    "Partition \"" << name << "\" already exist",
			    ((std::string)name )
		     )
}
    
int main (int argc, char ** argv)
{
    std::list< std::pair< std::string, std::string > > opt;
    try {
	opt = IPCCore::extractOptions( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 2;
    }

    opt.push_front( std::make_pair( std::string("threadPerConnectionPolicy"), std::string("0") ) );
    opt.push_front( std::make_pair( std::string("maxServerThreadPoolSize"), std::string("9") ) );
    opt.push_front( std::make_pair( std::string("threadPoolWatchConnection"), std::string("0") ) );
    opt.push_front( std::make_pair( std::string("scanGranularity"), std::string("30") ) );
    
    CmdArgInt  port 		('P', "port", "port-number", "Tcp port to listen on.");
    CmdArgStr  ior_file 	('i', "ior", "ior-file", "File to output IOR to.");
    CmdArgStr  restore_from	('r', "restore", "restore-from-file", "File to restore from.");
    CmdArgStr  backup_dir	('d', "directory", "backup-directory", "Directory to store backup (default /tmp).");
    CmdArgStr  partition_name	('p', "partition", "partition-name", "Partition to create.");
    CmdArgBool print_initial_ref('R', "ref", "Print the IPC initial reference.");
    CmdArgBool verbose		('v', "verbose", "Run in verbose mode.");
    CmdArgBool restore		('r', "restore", "Restore previous state from the backup.");
    CmdArgBool backup		('b', "backup", "Use backup facility.");
    CmdArgBool corbaloc		('C', "corbaloc", "Print IOR in corbaloc format.");
    CmdArgBool pmg_sync		('s', "sync", "use PMG synchronization.");
    CmdArgBool mirror		('m', "mirror", "run in mirroring mode.");
    CmdArgBool no_write		('N', "no-write", "do not try to store initial reference.");
    CmdArgInt  checkpoint_period('u', "update", "checkpoint-period", 
    		"How often the backup file checkpoint is done (default 1 second).");
    CmdArgInt  busy_threshold	('t', "threshold", "busy-threshold", 
    		"Do backup only when request rate is less then the threshold (default 500Hz).");
                
    // Declare command object and its argument-iterator
    CmdLine  cmd(*argv, &port, &partition_name, &ior_file, &backup_dir, &pmg_sync,
    			&print_initial_ref, &verbose, &restore, &backup, &corbaloc, 
                        &checkpoint_period, &busy_threshold, &mirror, &no_write, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

    cmd.description(	"CORBA Name Service implementation."
    			"Represents a partition for TDAQ applications." );

    // Parse arguments
    
    verbose = 0;
    port = -1;
    pmg_sync = false;
    corbaloc = false;
    no_write = false;
    backup_dir = "/tmp";
    checkpoint_period = 1;
    busy_threshold = 500;
    
    cmd.parse(arg_iter);
    
    //////////////////////////////////////////
    // set verbosity
    //////////////////////////////////////////
    if ( verbose )
    {
    	ers::Configuration::instance().debug_level( 2 );
    }
    
    //////////////////////////////////////////
    // add port option
    //////////////////////////////////////////
    if ( port != -1 )
    {
	std::ostringstream out;
	out << "giop:tcp::" << port;
        opt.push_front( std::make_pair( std::string("endPoint"), out.str() ) );
    }
    
    //////////////////////////////////////////
    // Initialise ORB
    //////////////////////////////////////////
    try {
        IPCCore::init( opt );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::fatal( ex );
	return 1;
    }
        
    //////////////////////////////////////////////////
    // Print location of the IPC reference file
    //////////////////////////////////////////////////
    if ( print_initial_ref )
    {
	ERS_LOG( "Initial reference is " << ipc::util::getInitialReference() );
    }
    
    std::string IOR;
    //////////////////////////////////////////////////
    // Create partition object
    //////////////////////////////////////////////////
    Partition_impl * implementation = 0;
    Partition_impl * mirror_implementation = 0;
    std::string name;
    if ( partition_name.flags() && CmdArg::GIVEN )
    {
	name = (const char*)partition_name;
        if ( mirror )
        {
	    //////////////////////////////////////////////////
	    // Check if partition already exist
	    //////////////////////////////////////////////////
	    IPCPartition partition( name + ipc::partition::mirror_postfix );
	    if ( partition.isValid() )
	    {
		ers::fatal( daq::ipc::PartitionAlreadyExist( ERS_HERE, partition.name() ) );
		return 3;
	    }
            
	    implementation = new Partition_impl( 
            		(const char*)backup_dir, checkpoint_period, busy_threshold, restore, backup );
            try {
            	implementation -> publish( name + ipc::partition::mirror_postfix );
		
                mirror_implementation = new Partition_impl( name, 
                		(const char*)backup_dir, checkpoint_period, busy_threshold, restore, backup );
                mirror_implementation -> publish( name + ipc::partition::mirror_postfix, name );
                mirror_implementation -> publish( name + ipc::partition::mirror_postfix );
            }
            catch ( daq::ipc::Exception & ex ) {
            	ers::fatal( ex );
                return 4;
            }
        }
        else
        {
	    //////////////////////////////////////////////////
	    // Check if partition already exist
	    //////////////////////////////////////////////////
	    IPCPartition partition( name );
	    if ( partition.isValid() )
	    {
		ers::fatal( daq::ipc::PartitionAlreadyExist( ERS_HERE, partition.name() ) );
		return 3;
	    }
            
	    //////////////////////////////////////////
	    // Named partition is created
	    //////////////////////////////////////////
	    implementation = new Partition_impl( name, 
            	(const char*)backup_dir, checkpoint_period, busy_threshold, restore, backup );

	    try {
		implementation -> publish();
	    }
	    catch ( daq::ipc::Exception & ex ) {
		ers::fatal( ex );
		return 5;
	    }
	}
    }
    else
    {
	//////////////////////////////////////////////////
	// Check if partition already exist
	//////////////////////////////////////////////////
	IPCPartition partition;
	if ( partition.isValid() )
	{
	    ers::fatal( daq::ipc::PartitionAlreadyExist( ERS_HERE, partition.name() ) );
	    return 3;
	}

	//////////////////////////////////////////
	// Root (default) partition is created
	//////////////////////////////////////////
	implementation = new Partition_impl( 
        	(const char*)backup_dir, checkpoint_period, busy_threshold, restore, backup );
    }

    ////////////////////////////////////////////////////////
    // Create IOR reference
    ////////////////////////////////////////////////////////
    CORBA::Object_var reference = implementation -> _this();
        
    try {
        IOR = IPCCore::objectToString( reference, corbaloc ? IPCCore::Corbaloc : IPCCore::Ior );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::error( ex );
    }
    
    if ( !( partition_name.flags() && CmdArg::GIVEN ) && !( no_write.flags() && CmdArg::GIVEN ) )
    {
	////////////////////////////////////////////////////////////////
	// Root (default) partition: store IOR as default reference
	////////////////////////////////////////////////////////////////
    	try {
	    ipc::util::storeAsInitialReference( IOR );
	}
	catch( daq::ipc::FileException & ex ) {
	    ers::fatal( ex );
	    return 6;
	}
	catch( daq::ipc::BadReferenceFormat & ex ) {
	    ers::fatal( ex );
	    return 6;
	}
	catch( daq::ipc::NotFileReference & ex ) {
	    ers::warning( ex );
	}
    }                

    ////////////////////////////////////////////////////////
    // Print IOR to stdout or to file
    ////////////////////////////////////////////////////////
    if ( ior_file.flags() && CmdArg::GIVEN )
    {
	if ( !strcmp(ior_file, "-") )
        {
	    std::cout << IOR << std::endl << std::flush;
        }
	else
	{
	    std::ofstream fout(ior_file);
	    if ( !fout )
	    {
		ers::fatal( daq::ipc::CannotOpenFile( ERS_HERE, (const char*)ior_file, strerror(errno) ) );
		return 5;
	    }
            else
            {
		fout << IOR << std::flush;
		if ( fout.fail() || fout.bad() )
                {
		    ers::fatal( daq::ipc::CannotOpenFile( ERS_HERE, (const char*)ior_file, "unknown" ) );
                    return 5;
                }
            }
	}
    }

    //////////////////////////////////////////
    // Tell to PMG that this program is ready
    //////////////////////////////////////////
    if ( pmg_sync.flags() && CmdArg::GIVEN )
    {
        pmg_initSync();
    }
    
    ERS_LOG( "ipc_server for partition \"" << implementation->name() << "\" has been started." );
    
    //////////////////////////////////////////
    // Block the current thread
    //////////////////////////////////////////
    daq::ipc::signal::wait_for();
    
    ERS_LOG( "ipc_server for partition \"" << implementation->name() << "\" has been stopped." );
    
    implementation -> _destroy( true );
    implementation = 0;
    
    if ( mirror_implementation )
    {
	mirror_implementation -> withdraw( name + ipc::partition::mirror_postfix );
	mirror_implementation -> _destroy( true );
        mirror_implementation = 0;
    }
    
    return 0;
}
