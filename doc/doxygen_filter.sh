#! /bin/bash

cpp	-nostdinc -nostdinc++ -imacros boost/date_time/posix_time/posix_time.hpp -imacros ers/Assertion.h \
	-I/afs/cern.ch/atlas/project/tdaq/cmt/tdaq-common-01-04-00/installed/include \
        -I/afs/cern.ch/sw/lcg/external/Boost/1.33.1/slc3_ia32_gcc323/include/boost-1_33_1 -P -C $1 \
        | sed -e '/^\n*\t* *$/ {d}'
