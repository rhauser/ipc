#!/bin/sh
#################################################################
#
#	Check target script for the IS package
#	Created by Sergei Kolos; 26.02.02
#
#################################################################

test_script_result()
{
  if test $? -ne 0
  then
	echo "*************** Check target failed ***************"
	ipc_rm -i ".*" -n ".*" -t -f
	exit 1
  fi
}

directory=$1
shift 1

LD_LIBRARY_PATH=$1:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

${directory}/run.sh $@
test_script_result

${directory}/run.sh $@ "WITH PROXY"
test_script_result
